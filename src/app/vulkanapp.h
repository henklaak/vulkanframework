#ifndef VULKANAPP_H
#define VULKANAPP_H

#include <vulkanbase.h>
#include <VulkanglTFModel.h>

class VulkanExample: public VulkanBase
{
public:
    VulkanExample( bool validation );
    virtual ~VulkanExample();
    void prepare() override;

protected:
    void getEnabledFeatures() override;
    void buildCommandBuffers() override;
    void render() override;

private:
    void loadAssets();
    void setupDescriptors();
    void preparePipelines();
    void prepareUniformBuffers();
    void updateUniformBuffers();
    void draw();

    struct Object
    {
        struct Matrices
        {
            glm::mat4 projection;
            glm::mat4 view;
            glm::mat4 model;
            glm::vec4 lightPos;
        } matrices;
        VkDescriptorSet descriptorSet;
        vks::Buffer uniformBuffer;
        glm::vec3 rotation;
    };
    Object m_object;

    vkglTF::Model m_model;

    VkPipeline m_pipeline;
    VkPipelineLayout m_pipelineLayout;

    VkDescriptorSetLayout m_descriptorSetLayout;

    bool m_animate = true;

};

#endif
