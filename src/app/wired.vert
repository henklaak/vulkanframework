#version 450

#extension GL_EXT_multiview : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 inUV;

layout (binding = 0) uniform UBO
{
    mat4 projectionMatrix[2];
    mat4 viewMatrix[2];
    mat4 modelMatrix;
} ubo;

layout (location = 0) out vec4 outColor;

out gl_PerVertex
{
    vec4 gl_Position;
};


void main()
{
    outColor = vec4(inColor,1);
    gl_Position =
            ubo.projectionMatrix[gl_ViewIndex] *
            ubo.viewMatrix[gl_ViewIndex] *
            ubo.modelMatrix *
            vec4(inPos.xyz, 1.0);
}
