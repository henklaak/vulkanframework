#include "vulkanapp.h"
#include <vulkanbase.h>
#include <cstddef>
#include "basicpipeline.h"
#include "triangle.h"

/**************************************************************************************************/
VulkanExample::VulkanExample( bool validation )
    : VulkanBase( validation )
{
    title = "Multiview rendering";
    camera.type = Camera::CameraType::lookat;
    camera.setPosition( glm::vec3( 0.0f, 0.0f, -2.0f ) );
    camera.setRotation( glm::vec3( 0.0f ) );
    camera.setPerspective( 45.0f, ( float )width / ( float )height, 0.01f, 256.0f );
}

/**************************************************************************************************/
VulkanExample::~VulkanExample()
{
}

/**************************************************************************************************/
void VulkanExample::prepare()
{
//    vr::EVRInitError eError = vr::VRInitError_None;
//    m_pHMD = vr::VR_Init( &eError, vr::VRApplication_Scene );
//    assert( eError == vr::VRInitError_None );

//    m_pRenderModels = ( vr::IVRRenderModels * )vr::VR_GetGenericInterface(
//                          vr::IVRRenderModels_Version,
//                          &eError );
//    assert( eError == vr::VRInitError_None );

    VulkanBase::prepare();
//    //prepareMultiview();
//    prepareVertices();
//    prepareUniformBuffers();
//    prepareDescriptors();
    preparePipelines();
    buildCommandBuffers();


    prepared = true;
}

/**************************************************************************************************/
void VulkanExample::prepareVertices()
{
//    // TODO staging to device local memory
//    std::vector<Vertex> vertexData =
//    {
//        {{  1.0f,  1.0f, 0.0f }, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
//        {{  1.0f, -1.0f, 0.0f }, {1.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
//        {{ -1.0f, -1.0f, 0.0f }, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
//        {{ -1.0f,  1.0f, 0.0f }, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
//    };

//    VK_CHECK_RESULT( vulkanDevice->createBuffer(
//                         VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
//                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
//                         &vertexBuffer,
//                         vertexData.size() * sizeof( Vertex ),
//                         vertexData.data() ) );

//    /*******/

//    std::vector<uint32_t> indexData = { 0, 1, 2, 2, 3, 0};

//    VK_CHECK_RESULT( vulkanDevice->createBuffer(
//                         VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
//                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
//                         &indexBuffer,
//                         indexData.size() * sizeof( uint32_t ),
//                         indexData.data() ) );
}

/**************************************************************************************************/
void VulkanExample::prepareUniformBuffers()
{
//    VK_CHECK_RESULT( vulkanDevice->createBuffer(
//                         VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
//                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
//                         &uniformBufferVS,
//                         sizeof( uboVS ) ) );
//    VK_CHECK_RESULT( uniformBufferVS.map() );
//    updateUniformBuffers();

//    // TODO textures
}

/**************************************************************************************************/
#if 0
void VulkanExample::prepareMultiview()
{
    // Example renders to two views (left/right)
    const uint32_t multiviewLayerCount = 2;

    /*
        Layered depth/stencil framebuffer
    */
    {
        VkImageCreateInfo imageCI = vks::initializers::imageCreateInfo();
        imageCI.imageType = VK_IMAGE_TYPE_2D;
        imageCI.format = m_depthFormat;
        imageCI.extent = { width, height, 1 };
        imageCI.mipLevels = 1;
        imageCI.arrayLayers = multiviewLayerCount;
        imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
        imageCI.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        imageCI.flags = 0;
        VK_CHECK_RESULT( vkCreateImage( m_device, &imageCI, nullptr, &multiviewPass.depth.image ) );

        VkMemoryRequirements memReqs;
        vkGetImageMemoryRequirements( m_device, multiviewPass.depth.image, &memReqs );

        VkMemoryAllocateInfo memAllocInfo{};
        memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        memAllocInfo.allocationSize = 0;
        memAllocInfo.memoryTypeIndex = 0;

        VkImageViewCreateInfo depthStencilView = {};
        depthStencilView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        depthStencilView.pNext = NULL;
        depthStencilView.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        depthStencilView.format = m_depthFormat;
        depthStencilView.flags = 0;
        depthStencilView.subresourceRange = {};
        depthStencilView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT |
                VK_IMAGE_ASPECT_STENCIL_BIT;
        depthStencilView.subresourceRange.baseMipLevel = 0;
        depthStencilView.subresourceRange.levelCount = 1;
        depthStencilView.subresourceRange.baseArrayLayer = 0;
        depthStencilView.subresourceRange.layerCount = multiviewLayerCount;
        depthStencilView.image = multiviewPass.depth.image;

        memAllocInfo.allocationSize = memReqs.size;
        memAllocInfo.memoryTypeIndex = vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                                       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );
        VK_CHECK_RESULT( vkAllocateMemory( m_device, &memAllocInfo, nullptr,
                                           &multiviewPass.depth.memory ) );
        VK_CHECK_RESULT( vkBindImageMemory( m_device, multiviewPass.depth.image, multiviewPass.depth.memory,
                                            0 ) );
        VK_CHECK_RESULT( vkCreateImageView( m_device, &depthStencilView, nullptr,
                                            &multiviewPass.depth.view ) );
    }

    /*
        Layered color attachment
    */
    {
        VkImageCreateInfo imageCI = vks::initializers::imageCreateInfo();
        imageCI.imageType = VK_IMAGE_TYPE_2D;
        imageCI.format = m_swapChain.colorFormat;
        imageCI.extent = { width, height, 1 };
        imageCI.mipLevels = 1;
        imageCI.arrayLayers = multiviewLayerCount;
        imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
        imageCI.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        VK_CHECK_RESULT( vkCreateImage( m_device, &imageCI, nullptr, &multiviewPass.color.image ) );

        VkMemoryRequirements memReqs;
        vkGetImageMemoryRequirements( m_device, multiviewPass.color.image, &memReqs );

        VkMemoryAllocateInfo memoryAllocInfo = vks::initializers::memoryAllocateInfo();
        memoryAllocInfo.allocationSize = memReqs.size;
        memoryAllocInfo.memoryTypeIndex = vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );
        VK_CHECK_RESULT( vkAllocateMemory( m_device, &memoryAllocInfo, nullptr,
                                           &multiviewPass.color.memory ) );
        VK_CHECK_RESULT( vkBindImageMemory( m_device, multiviewPass.color.image, multiviewPass.color.memory,
                                            0 ) );

        VkImageViewCreateInfo imageViewCI = vks::initializers::imageViewCreateInfo();
        imageViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        imageViewCI.format = m_swapChain.colorFormat;
        imageViewCI.flags = 0;
        imageViewCI.subresourceRange = {};
        imageViewCI.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCI.subresourceRange.baseMipLevel = 0;
        imageViewCI.subresourceRange.levelCount = 1;
        imageViewCI.subresourceRange.baseArrayLayer = 0;
        imageViewCI.subresourceRange.layerCount = multiviewLayerCount;
        imageViewCI.image = multiviewPass.color.image;
        VK_CHECK_RESULT( vkCreateImageView( m_device, &imageViewCI, nullptr, &multiviewPass.color.view ) );

        // Create sampler to sample from the attachment in the fragment shader
        VkSamplerCreateInfo samplerCI = vks::initializers::samplerCreateInfo();
        samplerCI.magFilter = VK_FILTER_NEAREST;
        samplerCI.minFilter = VK_FILTER_NEAREST;
        samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerCI.addressModeV = samplerCI.addressModeU;
        samplerCI.addressModeW = samplerCI.addressModeU;
        samplerCI.mipLodBias = 0.0f;
        samplerCI.maxAnisotropy = 1.0f;
        samplerCI.minLod = 0.0f;
        samplerCI.maxLod = 1.0f;
        samplerCI.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
        VK_CHECK_RESULT( vkCreateSampler( m_device, &samplerCI, nullptr, &multiviewPass.sampler ) );

        // Fill a descriptor for later use in a descriptor set
        multiviewPass.descriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        multiviewPass.descriptor.imageView = multiviewPass.color.view;
        multiviewPass.descriptor.sampler = multiviewPass.sampler;
    }

    /*
        Renderpass
    */
    {
        std::array<VkAttachmentDescription, 2> attachments = {};
        // Color attachment
        attachments[0].format = m_swapChain.colorFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        // Depth attachment
        attachments[1].format = m_depthFormat;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference colorReference = {};
        colorReference.attachment = 0;
        colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depthReference = {};
        depthReference.attachment = 1;
        depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpassDescription = {};
        subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpassDescription.colorAttachmentCount = 1;
        subpassDescription.pColorAttachments = &colorReference;
        subpassDescription.pDepthStencilAttachment = &depthReference;

        // Subpass dependencies for layout transitions
        std::array<VkSubpassDependency, 2> dependencies;

        dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[0].dstSubpass = 0;
        dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        dependencies[1].srcSubpass = 0;
        dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        VkRenderPassCreateInfo renderPassCI{};
        renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassCI.attachmentCount = static_cast<uint32_t>( attachments.size() );
        renderPassCI.pAttachments = attachments.data();
        renderPassCI.subpassCount = 1;
        renderPassCI.pSubpasses = &subpassDescription;
        renderPassCI.dependencyCount = static_cast<uint32_t>( dependencies.size() );
        renderPassCI.pDependencies = dependencies.data();

//        /*
//            Setup multiview info for the renderpass
//        */

//        /*
//            Bit mask that specifies which view rendering is broadcast to
//            0011 = Broadcast to first and second view (layer)
//        */
//        const uint32_t viewMask = 0b00000011;

//        /*
//            Bit mask that specifies correlation between views
//            An implementation may use this for optimizations (concurrent render)
//        */
//        const uint32_t correlationMask = 0b00000011;

//        VkRenderPassMultiviewCreateInfo renderPassMultiviewCI{};
//        renderPassMultiviewCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
//        renderPassMultiviewCI.subpassCount = 1;
//        renderPassMultiviewCI.pViewMasks = &viewMask;
//        renderPassMultiviewCI.correlationMaskCount = 1;
//        renderPassMultiviewCI.pCorrelationMasks = &correlationMask;

//        renderPassCI.pNext = &renderPassMultiviewCI;

//        VK_CHECK_RESULT( vkCreateRenderPass( device, &renderPassCI, nullptr, &multiviewPass.renderPass ) );
    }

    /*
        Framebuffer
    */
    {
        VkImageView attachments[2];
        attachments[0] = multiviewPass.color.view;
        attachments[1] = multiviewPass.depth.view;

        VkFramebufferCreateInfo framebufferCI = vks::initializers::framebufferCreateInfo();
        framebufferCI.renderPass = multiviewPass.renderPass;
        framebufferCI.attachmentCount = 2;
        framebufferCI.pAttachments = attachments;
        framebufferCI.width = width;
        framebufferCI.height = height;
        framebufferCI.layers = 1;
        VK_CHECK_RESULT( vkCreateFramebuffer( m_device, &framebufferCI, nullptr,
                                              &multiviewPass.frameBuffer ) );
    }
}
#endif


/**************************************************************************************************/
void VulkanExample::prepareDescriptors()
{
#if 0
    /* descriptorPoolSizes */
    VkDescriptorPoolSize descriptorPoolSize {};
    descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorPoolSize.descriptorCount = 1;

    std::vector<VkDescriptorPoolSize> descriptorPoolSizes =
    {
        descriptorPoolSize
    };

    /* descriptorPoolCI */
    VkDescriptorPoolCreateInfo descriptorPoolCI{};
    descriptorPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCI.poolSizeCount = descriptorPoolSizes.size();
    descriptorPoolCI.pPoolSizes = descriptorPoolSizes.data();
    descriptorPoolCI.maxSets = 1;

    /* descriptorPool */
    VK_CHECK_RESULT( vkCreateDescriptorPool(
                         m_device,
                         &descriptorPoolCI,
                         nullptr,
                         &m_descriptorPool ) );

    /* descriptorSetLayoutBindings */
    VkDescriptorSetLayoutBinding descriptorSetLayoutBinding {};
    descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    descriptorSetLayoutBinding.binding = 0;
    descriptorSetLayoutBinding.descriptorCount = 1;

    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings =
    {
        descriptorSetLayoutBinding
    };

    /* descriptorSetLayoutCI */
    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCI{};
    descriptorSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCI.pBindings = descriptorSetLayoutBindings.data();
    descriptorSetLayoutCI.bindingCount = descriptorSetLayoutBindings.size();

    /* descriptorSetLayouts */
    VK_CHECK_RESULT( vkCreateDescriptorSetLayout(
                         m_device,
                         &descriptorSetLayoutCI,
                         nullptr,
                         &descriptorSetLayout ) );

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts =
    {
        descriptorSetLayout
    };

    /* pipelineLayoutCI */
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCI.setLayoutCount = descriptorSetLayouts.size();
    pipelineLayoutCI.pSetLayouts = descriptorSetLayouts.data();

    /* pipelineLayout */
    VK_CHECK_RESULT( vkCreatePipelineLayout(
                         m_device,
                         &pipelineLayoutCI,
                         nullptr,
                         &m_pipelineLayout ) );

    /* descriptorSetAllocateInfo */
    VkDescriptorSetAllocateInfo descriptorSetAI{};
    descriptorSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAI.descriptorPool = m_descriptorPool;
    descriptorSetAI.pSetLayouts = descriptorSetLayouts.data();
    descriptorSetAI.descriptorSetCount = 1;

    /* descriptorSet */
    VK_CHECK_RESULT( vkAllocateDescriptorSets(
                         m_device,
                         &descriptorSetAI,
                         &descriptorSet ) );

    /* writeDescriptorSets */
    VkWriteDescriptorSet writeDescriptorSet{};
    writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSet.dstSet = descriptorSet;
    writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writeDescriptorSet.dstBinding = 0;
    writeDescriptorSet.pBufferInfo = &( uniformBufferVS.descriptor ) ;
    writeDescriptorSet.descriptorCount = 1;

    std::vector<VkWriteDescriptorSet> writeDescriptorSets =
    {
        writeDescriptorSet
    };

    /* Update */
    vkUpdateDescriptorSets( m_device,
                            writeDescriptorSets.size(),
                            writeDescriptorSets.data(),
                            0,
                            nullptr );
#endif
}

/**************************************************************************************************/
void VulkanExample::preparePipelines()
{
//    VkSemaphoreCreateInfo semaphoreCI = vks::initializers::semaphoreCreateInfo();
//    VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreCI, nullptr, &multiviewPass.semaphore));

    m_pipelineLayout[PSO_COMPANION] = createBasicPipelineLayout(
                                          m_device,
                                          m_pipelineCache,
                                          m_frameRenderPass,
                                          width,
                                          height );
    m_pipeline[PSO_COMPANION] = createBasicPipeline(
                                    m_device,
                                    m_pipelineCache,
                                    m_pipelineLayout[PSO_COMPANION],
                                    m_frameRenderPass,
                                    width,
                                    height );

    return;
#if 0
    const char *pShaderNames[ PSO_COUNT ] =
    {
        "basic",
        "wired",
    };
    const char *pStageNames[ 2 ] =
    {
        "vert",
        "frag"
    };
    for( size_t pso = 0; pso < PSO_COUNT; ++pso )
    {
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCI{};
        inputAssemblyStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyStateCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssemblyStateCI.primitiveRestartEnable = VK_FALSE;

        VkPipelineRasterizationStateCreateInfo rasterizationStateCI{};
        rasterizationStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizationStateCI.depthClampEnable = VK_FALSE;
        rasterizationStateCI.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizationStateCI.cullMode = VK_CULL_MODE_NONE;
        rasterizationStateCI.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizationStateCI.lineWidth = 1.0f;

        VkPipelineColorBlendAttachmentState colorBlendAttachmentState {};
        colorBlendAttachmentState.blendEnable = VK_FALSE;
        colorBlendAttachmentState.colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT |
            VK_COLOR_COMPONENT_G_BIT |
            VK_COLOR_COMPONENT_B_BIT |
            VK_COLOR_COMPONENT_A_BIT;

        VkPipelineColorBlendStateCreateInfo colorBlendStateCI {};
        colorBlendStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendStateCI.attachmentCount = 1;
        colorBlendStateCI.pAttachments = &colorBlendAttachmentState;

        VkPipelineDepthStencilStateCreateInfo depthStencilStateCI {};
        depthStencilStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencilStateCI.depthTestEnable = VK_TRUE;
        depthStencilStateCI.depthWriteEnable = VK_TRUE;
        depthStencilStateCI.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;

        VkViewport viewPort{};
        viewPort.x = 0;
        viewPort.y = 0;
        viewPort.width = width;
        viewPort.height = height;
        viewPort.minDepth = 0;
        viewPort.maxDepth = 1;

        VkRect2D scissors{};
        scissors.offset.x = 0;
        scissors.offset.y = 0;
        scissors.extent.width = width;
        scissors.extent.height = height;

        VkPipelineViewportStateCreateInfo viewportStateCI {};
        viewportStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportStateCI.viewportCount = 1;
        viewportStateCI.pViewports = &viewPort;
        viewportStateCI.scissorCount = 1;
        viewportStateCI.pScissors = &scissors;

        VkPipelineMultisampleStateCreateInfo multisampleStateCI {};
        multisampleStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleStateCI.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

        std::vector<VkDynamicState> dynamicStateEnables = {};
        VkPipelineDynamicStateCreateInfo dynamicStateCI{};
        dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicStateCI.pDynamicStates = dynamicStateEnables.data();
        dynamicStateCI.dynamicStateCount = dynamicStateEnables.size();
        dynamicStateCI.flags = 0;

        /* Vertex */

        VkVertexInputBindingDescription bindingDescription{};
        bindingDescription.binding = 0;
        bindingDescription.stride = sizeof( Vertex );
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions{};
        attributeDescriptions[0].binding = 0;
        attributeDescriptions[0].location = 0;
        attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[0].offset = offsetof( Vertex, position );
        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof( Vertex, color );
        attributeDescriptions[2].binding = 0;
        attributeDescriptions[2].location = 2;
        attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
        attributeDescriptions[2].offset = offsetof( Vertex, uv );

        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
        vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();

        std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages;
        char shaderFileName[ 1024 ];

        sprintf( shaderFileName, "%s.%s.spv", pShaderNames[ pso ], pStageNames[ 0 ] );
        shaderStages[0] = loadShader( shaderFileName, VK_SHADER_STAGE_VERTEX_BIT );

        sprintf( shaderFileName, "%s.%s.spv", pShaderNames[ pso ], pStageNames[ 1 ] );
        shaderStages[1] = loadShader( shaderFileName, VK_SHADER_STAGE_FRAGMENT_BIT );

        /* pipelineCI */
        VkGraphicsPipelineCreateInfo pipelineCI {};
        pipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineCI.layout = pipelineLayout;
        pipelineCI.renderPass = m_renderPass;
        pipelineCI.flags = 0;
        pipelineCI.basePipelineIndex = -1;
        pipelineCI.basePipelineHandle = VK_NULL_HANDLE;
        pipelineCI.pInputAssemblyState = &inputAssemblyStateCI;
        pipelineCI.pRasterizationState = &rasterizationStateCI;
        pipelineCI.pColorBlendState = &colorBlendStateCI;
        pipelineCI.pMultisampleState = &multisampleStateCI;
        pipelineCI.pViewportState = &viewportStateCI;
        pipelineCI.pDepthStencilState = &depthStencilStateCI;
        pipelineCI.pDynamicState = &dynamicStateCI;
        pipelineCI.pVertexInputState  = &vertexInputInfo;
        pipelineCI.pStages = shaderStages.data();
        pipelineCI.stageCount = 2;

        VK_CHECK_RESULT( vkCreateGraphicsPipelines( m_device, m_pipelineCache, 1, &pipelineCI, nullptr,
                         &( pipeline[pso] ) ) );
    }
#endif
}

/**************************************************************************************************/
void VulkanExample::buildCommandBuffers()
{



    for( size_t i = 0; i < m_drawCmdBuffers.size(); ++i )
    {
        buildTriangleCommandBuffer(
            m_frameRenderPass,
            m_drawCmdBuffers[i],
            m_frameBuffers[i],
            m_pipelineLayout,
            width,
            height );


//        renderPassBeginInfo.framebuffer = m_frameBuffers[i];

//        vkCmdBeginRenderPass( m_drawCmdBuffers[i],
//                              &renderPassBeginInfo,
//                              VK_SUBPASS_CONTENTS_INLINE );

//        for( size_t pso = 0; pso < PSO_COUNT; ++pso )
//        {
//            vkCmdBindPipeline( m_drawCmdBuffers[i],
//                               VK_PIPELINE_BIND_POINT_GRAPHICS,
//                               pipeline[pso] );

//            vkCmdBindDescriptorSets( m_drawCmdBuffers[i],
//                                     VK_PIPELINE_BIND_POINT_GRAPHICS,
//                                     m_pipelineLayout,
//                                     0,
//                                     1, &descriptorSet,
//                                     0, nullptr );

//            // Bind triangle vertex buffer (contains position and colors)
//            VkDeviceSize offsets {};
//            vkCmdBindVertexBuffers( m_drawCmdBuffers[i],
//                                    0,
//                                    1,
//                                    &vertexBuffer.buffer,
//                                    &offsets );

//            // Bind triangle index buffer
//            vkCmdBindIndexBuffer( m_drawCmdBuffers[i],
//                                  indexBuffer.buffer,
//                                  0,
//                                  VK_INDEX_TYPE_UINT32 );

//            // Draw indexed triangle
//            vkCmdDrawIndexed( m_drawCmdBuffers[i],
//                              indexBuffer.size / sizeof( uint32_t ),
//                              1,
//                              0,
//                              0,
//                              0 );

//        }
//        vkCmdEndRenderPass( m_drawCmdBuffers[i] );
//        VK_CHECK_RESULT( vkEndCommandBuffer( m_drawCmdBuffers[i] ) );
    }

//    multiviewPass.commandBuffers.resize( drawCmdBuffers.size() );

//    VkCommandBufferAllocateInfo cmdBufAllocateInfo = vks::initializers::commandBufferAllocateInfo(
//                cmdPool, VK_COMMAND_BUFFER_LEVEL_PRIMARY, static_cast<uint32_t>( drawCmdBuffers.size() ) );
//    VK_CHECK_RESULT( vkAllocateCommandBuffers( device, &cmdBufAllocateInfo,
//                     multiviewPass.commandBuffers.data() ) );

//    {
//        VkCommandBufferBeginInfo cmdBufInfo = vks::initializers::commandBufferBeginInfo();

//        VkClearValue clearValues[2];
//        clearValues[0].color = defaultClearColor;
//        clearValues[1].depthStencil = { 1.0f, 0 };

//        VkRenderPassBeginInfo renderPassBeginInfo = vks::initializers::renderPassBeginInfo();
//        renderPassBeginInfo.renderPass = multiviewPass.renderPass;
//        renderPassBeginInfo.renderArea.offset.x = 0;
//        renderPassBeginInfo.renderArea.offset.y = 0;
//        renderPassBeginInfo.renderArea.extent.width = width;
//        renderPassBeginInfo.renderArea.extent.height = height;
//        renderPassBeginInfo.clearValueCount = 2;
//        renderPassBeginInfo.pClearValues = clearValues;

//        for( int32_t i = 0; i < multiviewPass.commandBuffers.size(); ++i )
//        {
//            renderPassBeginInfo.framebuffer = multiviewPass.frameBuffer;

//            VK_CHECK_RESULT( vkBeginCommandBuffer( multiviewPass.commandBuffers[i], &cmdBufInfo ) );
//            vkCmdBeginRenderPass( multiviewPass.commandBuffers[i], &renderPassBeginInfo,
//                                  VK_SUBPASS_CONTENTS_INLINE );
//            VkViewport viewport = vks::initializers::viewport( ( float )width, ( float )height, 0.0f, 1.0f );
//            vkCmdSetViewport( multiviewPass.commandBuffers[i], 0, 1, &viewport );
//            VkRect2D scissor = vks::initializers::rect2D( width, height, 0, 0 );
//            vkCmdSetScissor( multiviewPass.commandBuffers[i], 0, 1, &scissor );

//            vkCmdBindPipeline( multiviewPass.commandBuffers[i],
//                               VK_PIPELINE_BIND_POINT_GRAPHICS,
//                               pipeline[0] );

//            vkCmdBindDescriptorSets( multiviewPass.commandBuffers[i],
//                                     VK_PIPELINE_BIND_POINT_GRAPHICS,
//                                     pipelineLayout,
//                                     0,
//                                     1, &descriptorSet,
//                                     0, nullptr );

//            // Bind triangle vertex buffer (contains position and colors)
//            VkDeviceSize offsets {};
//            vkCmdBindVertexBuffers( multiviewPass.commandBuffers[i],
//                                    0,
//                                    1,
//                                    &vertexBuffer.buffer,
//                                    &offsets );

//            // Bind triangle index buffer
//            vkCmdBindIndexBuffer( multiviewPass.commandBuffers[i],
//                                  indexBuffer.buffer,
//                                  0,
//                                  VK_INDEX_TYPE_UINT32 );

//            // Draw indexed triangle
//            vkCmdDrawIndexed( multiviewPass.commandBuffers[i],
//                              indexBuffer.size / sizeof( uint32_t ),
//                              1,
//                              0,
//                              0,
//                              0 );


//            vkCmdEndRenderPass( multiviewPass.commandBuffers[i] );
//            VK_CHECK_RESULT( vkEndCommandBuffer( multiviewPass.commandBuffers[i] ) );
//        }
//    }
}

/**************************************************************************************************/
void VulkanExample::render()
{
    if( !prepared )
    {
        return;
    }
    if( camera.updated )
    {
        //updateUniformBuffers();
    }
    draw();
}

/**************************************************************************************************/
void VulkanExample::draw()
{
    VulkanBase::prepareFrame();

//    // Multiview offscreen render
//    VK_CHECK_RESULT( vkWaitForFences( device,
//                                      1,
//                                      &multiviewPass.waitFences[currentBuffer],
//                                      VK_TRUE,
//                                      UINT64_MAX ) );
//    VK_CHECK_RESULT( vkResetFences( device,
//                                    1,
//                                    &multiviewPass.waitFences[currentBuffer] ) );
//    submitInfo.pWaitSemaphores = &semaphores.presentComplete;
//    submitInfo.pSignalSemaphores = &multiviewPass.semaphore;
//    submitInfo.commandBufferCount = 1;
//    submitInfo.pCommandBuffers = &multiviewPass.commandBuffers[currentBuffer];
//    VK_CHECK_RESULT( vkQueueSubmit( queue,
//                                    1,
//                                    &submitInfo,
//                                    multiviewPass.waitFences[currentBuffer] ) );

    // View display
    VK_CHECK_RESULT( vkWaitForFences( m_device,
                                      1,
                                      &m_waitFences[m_currentBuffer],
                                      VK_TRUE,
                                      UINT64_MAX ) );
    VK_CHECK_RESULT( vkResetFences( m_device,
                                    1,
                                    &m_waitFences[m_currentBuffer] ) );
    m_submitInfo.pWaitSemaphores = &m_semaphores.presentComplete;
    m_submitInfo.pSignalSemaphores = &m_semaphores.renderComplete;
    m_submitInfo.commandBufferCount = 1;
    m_submitInfo.pCommandBuffers = &m_drawCmdBuffers[m_currentBuffer];
    VK_CHECK_RESULT( vkQueueSubmit( m_graphics_queue,
                                    1,
                                    &m_submitInfo,
                                    m_waitFences[m_currentBuffer] ) );

    VulkanBase::submitFrame();
}

/**************************************************************************************************/
void VulkanExample::updateUniformBuffers()
{
//    uboVS.projection[0] = camera.matrices.perspective;
//    uboVS.projection[1] = camera.matrices.perspective;
//    uboVS.view[0] = camera.matrices.view;
//    uboVS.view[1] = camera.matrices.view;
//    uboVS.model = glm::mat4( 1.0f );

//    memcpy( uniformBufferVS.mapped, &uboVS, sizeof( uboVS ) );
}

/**************************************************************************************************/
int main( const int, const char *[] )
{
    VulkanExample *vulkanExample;
    vulkanExample = new VulkanExample( true );
    vulkanExample->settings.vsync = true;

    vulkanExample->initVulkan();
    vulkanExample->setupWindow();
    vulkanExample->prepare();
    vulkanExample->renderLoop();
    delete( vulkanExample );

    return 0;
}


