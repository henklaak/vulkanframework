#include "vulkanapp.h"
#include <VulkanglTFModel.h>


/**************************************************************************************************/
VulkanExample::VulkanExample( bool validation )
    : VulkanBase( validation )
{
    m_title = "Using descriptor Sets";
    m_camera.type = Camera::CameraType::lookat;
    m_camera.setPerspective( 60.0f, ( float )m_width / ( float )m_height, 0.1f, 512.0f );
    m_camera.setRotation( glm::vec3( 0.0f, 0.0f, 0.0f ) );
    m_camera.setTranslation( glm::vec3( 2.0f, 0.0f, -2.0f ) );

    // Enable extension required for multiview
    //m_enabledDeviceExtensions.push_back( VK_KHR_MULTIVIEW_EXTENSION_NAME );

    // Reading device properties and features for multiview requires VK_KHR_get_physical_device_properties2 to be enabled
    //m_enabledInstanceExtensions.push_back( VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME );
    //m_enabledInstanceExtensions.push_back( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
    m_enabledInstanceExtensions.push_back( VK_EXT_DEBUG_REPORT_EXTENSION_NAME );
}

/**************************************************************************************************/
VulkanExample::~VulkanExample()
{
    vkDestroyPipeline( m_device, m_pipeline, nullptr );
    vkDestroyPipelineLayout( m_device, m_pipelineLayout, nullptr );
    vkDestroyDescriptorSetLayout( m_device, m_descriptorSetLayout, nullptr );

    m_object.uniformBuffer.destroy();
}

/**************************************************************************************************/
void VulkanExample::getEnabledFeatures()
{
    if( m_deviceFeatures.samplerAnisotropy )
    {
        m_enabledFeatures.samplerAnisotropy = VK_TRUE;
    };
}

/**************************************************************************************************/
void VulkanExample::loadAssets()
{
    const uint32_t glTFLoadingFlags = vkglTF::FileLoadingFlags::PreTransformVertices |
                                      vkglTF::FileLoadingFlags::FlipY;
    m_model.loadFromFile(
        "voyager.gltf",
        m_vulkanDevice, m_graphicsQueue,
        glTFLoadingFlags,
        m_enabledFeatures.samplerAnisotropy );

    vks::debugmarker::setObjectName( m_device,
                                     ( uint64_t )m_model.indices.buffer,
                                     VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT,
                                     "bufIndices" );
    vks::debugmarker::setObjectName( m_device,
                                     ( uint64_t )m_model.vertices.buffer,
                                     VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT,
                                     "bufVertices" );

}

/**************************************************************************************************/
void VulkanExample::prepareUniformBuffers()
{
    // Vertex shader matrix uniform buffer block
    VK_CHECK_RESULT( m_vulkanDevice->createBuffer(
                         VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                         VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                         &m_object.uniformBuffer,
                         sizeof( Object::Matrices ) ) );
    VK_CHECK_RESULT( m_object.uniformBuffer.map() );

    vks::debugmarker::setObjectName( m_device,
                                     ( uint64_t )m_object.uniformBuffer.buffer,
                                     VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT,
                                     "bufUniform" );


    updateUniformBuffers();
}

/**************************************************************************************************/
void VulkanExample::setupDescriptors()
{
    // Example uses one ubo and one combined image sampler
    std::vector<VkDescriptorPoolSize> poolSizes =
    {
        vks::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1),
        vks::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1),
    };

    VkDescriptorPoolCreateInfo descriptorPoolCI =
        vks::initializers::descriptorPoolCreateInfo(
            poolSizes.size(),
            poolSizes.data(),
            2);

    VK_CHECK_RESULT(vkCreateDescriptorPool(m_device, &descriptorPoolCI, nullptr, &m_descriptorPool));

    /////////////////

    std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings =
    {
        vks::initializers::descriptorSetLayoutBinding(
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            VK_SHADER_STAGE_VERTEX_BIT, 0 ),
    };
    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCI = vks::initializers::descriptorSetLayoutCreateInfo(setLayoutBindings);
    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(m_device, &descriptorSetLayoutCI, nullptr, &m_descriptorSetLayout));

    // Layout uses set 0 for passing vertex shader ubo and set 1 for fragment shader images (taken from glTF model)
    const std::vector<VkDescriptorSetLayout> setLayouts = {
        m_descriptorSetLayout,
        vkglTF::descriptorSetLayoutImage,
    };
    VkPipelineLayoutCreateInfo pPipelineLayoutCreateInfo = vks::initializers::pipelineLayoutCreateInfo(setLayouts.data(), 2);
    VK_CHECK_RESULT(vkCreatePipelineLayout(m_device, &pPipelineLayoutCreateInfo, nullptr, &m_pipelineLayout));

    // Create the descriptor set layout

    VkDescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocateInfo.descriptorPool = m_descriptorPool;
    allocateInfo.descriptorSetCount = 1;
    allocateInfo.pSetLayouts = &m_descriptorSetLayout;
    VK_CHECK_RESULT( vkAllocateDescriptorSets(
                         m_device,
                         &allocateInfo,
                         &m_object.descriptorSet ) );

    // Update the descriptor set with the actual descriptors matching shader bindings set in the layout

    std::array<VkWriteDescriptorSet, 1> writeDescriptorSets{};

    /*
        Binding 0: Object matrices Uniform buffer
    */
    writeDescriptorSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSets[0].dstSet = m_object.descriptorSet;
    writeDescriptorSets[0].dstBinding = 0;
    writeDescriptorSets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writeDescriptorSets[0].pBufferInfo = &m_object.uniformBuffer.descriptor;
    writeDescriptorSets[0].descriptorCount = 1;

    // Execute the writes to update descriptors for this set
    // Note that it's also possible to gather all writes and only run updates once, even for multiple sets
    // This is possible because each VkWriteDescriptorSet also contains the destination set to be updated
    // For simplicity we will update once per set instead

    vkUpdateDescriptorSets(
        m_device,
        writeDescriptorSets.size(),
        writeDescriptorSets.data(),
        0,
        nullptr );

}

/**************************************************************************************************/
void VulkanExample::preparePipelines()
{
    const std::vector<VkDynamicState> dynamicStateEnables = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCI =
        vks::initializers::pipelineInputAssemblyStateCreateInfo(
            VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            0,
            VK_FALSE );
    VkPipelineRasterizationStateCreateInfo rasterizationStateCI =
        vks::initializers::pipelineRasterizationStateCreateInfo(
            VK_POLYGON_MODE_FILL,
            VK_CULL_MODE_BACK_BIT,
            VK_FRONT_FACE_COUNTER_CLOCKWISE,
            0 );
    VkPipelineColorBlendAttachmentState blendAttachmentState =
        vks::initializers::pipelineColorBlendAttachmentState(
            0xf,
            VK_FALSE );
    VkPipelineColorBlendStateCreateInfo colorBlendStateCI =
        vks::initializers::pipelineColorBlendStateCreateInfo(
            1
            , &blendAttachmentState );
    VkPipelineDepthStencilStateCreateInfo depthStencilStateCI =
        vks::initializers::pipelineDepthStencilStateCreateInfo(
            VK_TRUE,
            VK_TRUE,
            VK_COMPARE_OP_LESS_OR_EQUAL );
    VkPipelineViewportStateCreateInfo viewportStateCI =
        vks::initializers::pipelineViewportStateCreateInfo(
            1,
            1,
            0 );
    VkPipelineMultisampleStateCreateInfo multisampleStateCI =
        vks::initializers::pipelineMultisampleStateCreateInfo(
            m_mssa );
    VkPipelineDynamicStateCreateInfo dynamicStateCI =
        vks::initializers::pipelineDynamicStateCreateInfo(
            dynamicStateEnables.data(),
            dynamicStateEnables.size(),
            0 );
    std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages;

    VkGraphicsPipelineCreateInfo pipelineCI =
        vks::initializers::pipelineCreateInfo(
            m_pipelineLayout,
            m_renderPass,
            0 );

    pipelineCI.pInputAssemblyState = &inputAssemblyStateCI;
    pipelineCI.pRasterizationState = &rasterizationStateCI;
    pipelineCI.pColorBlendState = &colorBlendStateCI;
    pipelineCI.pMultisampleState = &multisampleStateCI;
    pipelineCI.pViewportState = &viewportStateCI;
    pipelineCI.pDepthStencilState = &depthStencilStateCI;
    pipelineCI.pDynamicState = &dynamicStateCI;
    pipelineCI.stageCount =  shaderStages.size();
    pipelineCI.pStages = shaderStages.data();
    pipelineCI.pVertexInputState  = vkglTF::Vertex::getPipelineVertexInputState(
    {
        vkglTF::VertexComponent::Position,
        vkglTF::VertexComponent::Normal,
        vkglTF::VertexComponent::Color,
        vkglTF::VertexComponent::UV
    } );

    shaderStages[0] = loadShader( "basic.vert.spv",
                                  VK_SHADER_STAGE_VERTEX_BIT );
    shaderStages[1] = loadShader( "basic.frag.spv",
                                  VK_SHADER_STAGE_FRAGMENT_BIT );
    VK_CHECK_RESULT( vkCreateGraphicsPipelines(
                         m_device,
                         m_pipelineCache,
                         1,
                         &pipelineCI,
                         nullptr,
                         &m_pipeline ) );
}

/**************************************************************************************************/
void VulkanExample::buildCommandBuffers()
{
    VkCommandBufferBeginInfo cmdBufInfo = vks::initializers::commandBufferBeginInfo();

    VkClearValue clearValues[3];
    clearValues[0].color = defaultClearColor;
    clearValues[1].depthStencil = { 1.0f, 0 };
    clearValues[2].color = defaultClearColor;

    VkRenderPassBeginInfo renderPassBeginInfo = vks::initializers::renderPassBeginInfo();
    renderPassBeginInfo.renderPass = m_renderPass;
    renderPassBeginInfo.renderArea.offset.x = 0;
    renderPassBeginInfo.renderArea.offset.y = 0;
    renderPassBeginInfo.renderArea.extent.width = m_width;
    renderPassBeginInfo.renderArea.extent.height = m_height;
    renderPassBeginInfo.clearValueCount = 3;
    renderPassBeginInfo.pClearValues = clearValues;

    for( size_t i = 0; i < m_drawCmdBuffers.size(); ++i )
    {
        renderPassBeginInfo.framebuffer = m_frameBuffers[i];

        VK_CHECK_RESULT( vkBeginCommandBuffer( m_drawCmdBuffers[i], &cmdBufInfo ) );

        vkCmdBeginRenderPass( m_drawCmdBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE );


        VkViewport viewport = vks::initializers::viewport( ( float )m_width, ( float )m_height, 0.0f,
                              1.0f );
        vkCmdSetViewport( m_drawCmdBuffers[i], 0, 1, &viewport );

        VkRect2D scissor = vks::initializers::rect2D( m_width, m_height, 0, 0 );
        vkCmdSetScissor( m_drawCmdBuffers[i], 0, 1, &scissor );

        m_model.bindBuffers( m_drawCmdBuffers[i] );

        // Bind the cube's descriptor set. This tells the command buffer to use the uniform buffer and image set for this cube
        vkCmdBindDescriptorSets(
            m_drawCmdBuffers[i],
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            m_pipelineLayout,
            0,
            1,
            &m_object.descriptorSet,
            0,
            nullptr );

        vkCmdBindPipeline( m_drawCmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline );

        vks::debugmarker::beginRegion( m_drawCmdBuffers[i],
                                       "TROLOL",
                                       glm::vec4( 1.0f, 0.78f, 0.05f, 1.0f ) );
        m_model.draw( m_drawCmdBuffers[i],
                      vkglTF::RenderFlags::BindImages,
                      m_pipelineLayout );
        vks::debugmarker::endRegion( m_drawCmdBuffers[i] );

        vkCmdEndRenderPass( m_drawCmdBuffers[i] );

        VK_CHECK_RESULT( vkEndCommandBuffer( m_drawCmdBuffers[i] ) );
    }
}

/**************************************************************************************************/
void VulkanExample::prepare()
{
    VulkanBase::prepare();
    for( uint32_t i = 0; i < m_swapChain.imageCount; ++i )
    {
        vks::debugmarker::setObjectName( m_device,
                                         ( uint64_t )m_swapChain.buffers[i].image,
                                         VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT,
                                         ( std::string( "imgSwapChain" ) + std::to_string( i ) ).c_str() );
        vks::debugmarker::setObjectName( m_device,
                                         ( uint64_t )m_swapChain.buffers[i].view,
                                         VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT,
                                         ( std::string( "viewSwapChain" ) + std::to_string( i ) ).c_str() );
    }
    for( uint32_t i = 0; i < m_frameBuffers.size(); ++i )
    {
        vks::debugmarker::setObjectName( m_device,
                                         ( uint64_t )m_frameBuffers[i],
                                         VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT,
                                         ( std::string( "bufFrame" ) + std::to_string( i ) ).c_str() );
    }


    loadAssets();
    prepareUniformBuffers();
    setupDescriptors();
    preparePipelines();
    buildCommandBuffers();
    m_prepared = true;
}

/**************************************************************************************************/
void VulkanExample::draw()
{
    VulkanBase::prepareFrame();
    m_submitInfo.commandBufferCount = 1;
    m_submitInfo.pCommandBuffers = &m_drawCmdBuffers[m_currentBuffer];

    VK_CHECK_RESULT( vkQueueSubmit( m_graphicsQueue, 1, &m_submitInfo, VK_NULL_HANDLE ) );
    VulkanBase::submitFrame();
}

/**************************************************************************************************/
void VulkanExample::updateUniformBuffers()
{
    m_object.matrices.model = glm::translate(
                                  glm::mat4( 1.0f ),
                                  glm::vec3( -2.0f, 0.0f, 0.0f ) );

    m_object.matrices.projection = m_camera.matrices.perspective;
    m_object.matrices.view = m_camera.matrices.view;
    m_object.matrices.model = glm::rotate(
                                  m_object.matrices.model,
                                  glm::radians( m_object.rotation.x ),
                                  glm::vec3( 1.0f, 0.0f, 0.0f ) );
    m_object.matrices.model = glm::rotate(
                                  m_object.matrices.model,
                                  glm::radians( m_object.rotation.y ),
                                  glm::vec3( 0.0f, 1.0f, 0.0f ) );
    m_object.matrices.model = glm::rotate(
                                  m_object.matrices.model,
                                  glm::radians( m_object.rotation.z ),
                                  glm::vec3( 0.0f, 0.0f, 1.0f ) );
    m_object.matrices.model = glm::scale(
                                  m_object.matrices.model,
                                  glm::vec3( 0.25f ) );

    m_object.matrices.lightPos = glm::vec4( 10, 0, 0, 1 );
    memcpy( m_object.uniformBuffer.mapped, &m_object.matrices, sizeof( m_object.matrices ) );
}

/**************************************************************************************************/
void VulkanExample::render()
{
    if( !m_prepared )
    {
        return;
    }
    draw();
    if( m_animate )
    {
        m_object.rotation.x += 2.5f * m_frameTimer;
        if( m_object.rotation.x > 360.0f )
        {
            m_object.rotation.x -= 360.0f;
        }
    }
    if( ( m_camera.updated ) || ( m_animate ) )
    {
        updateUniformBuffers();
    }
}

/**************************************************************************************************/
int main( const int, const char *[] )
{
    VulkanExample *vulkanExample;
    vulkanExample = new VulkanExample( true );
    vulkanExample->initVulkan();
    vulkanExample->setupWindow();
    vulkanExample->prepare();
    vulkanExample->renderLoop();
    delete( vulkanExample );

    return 0;
}


