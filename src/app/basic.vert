#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inColor;
layout (location = 3) in vec2 inUV;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec3 outColor;
layout (location = 2) out vec3 outViewVec;
layout (location = 3) out vec3 outLightVec;
layout (location = 4) out vec2 outUV;

layout (binding = 0) uniform UBO
{
    mat4 projectionMatrix;
    mat4 viewMatrix;
    mat4 modelMatrix;
    vec4 lightPos;
} ubo;


void main()
{
    mat4 modelviewMatrix = ubo.viewMatrix *ubo.modelMatrix;

    outColor = inColor;
    outUV = inUV;
    outNormal = mat3(modelviewMatrix) * inNormal;

    vec4 pos = vec4(inPos.xyz, 1.0);
    vec4 worldPos = modelviewMatrix * pos;

    vec3 lPos = vec3(modelviewMatrix * ubo.lightPos);
    outLightVec = lPos - worldPos.xyz;
    outViewVec = -worldPos.xyz;

    gl_Position = ubo.projectionMatrix * worldPos;
}
