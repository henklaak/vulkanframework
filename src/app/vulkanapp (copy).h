#ifndef VULKANAPP_H
#define VULKANAPP_H

#include <vulkanbase.h>
#include <openvr/openvr.h>

enum PipelineStateObjectEnum_t
{
    PSO_COMPANION,
    PSO_COUNT
};

class VulkanExample: public VulkanBase
{
public:
    VulkanExample( bool validation );
    virtual ~VulkanExample();
    void prepare() override;

protected:
    void buildCommandBuffers() override;
    void render() override;

private:
    void prepareVertices();
    void prepareUniformBuffers();
    void prepareDescriptors();
    void preparePipelines();

    void draw();
    void updateUniformBuffers();


//    struct Vertex
//    {
//        float position[3];
//        float color[3];
//        float uv[2];
//    };
//    vks::Buffer vertexBuffer;
//    vks::Buffer indexBuffer;

//    struct UBO
//    {
//        glm::mat4 projection[2];
//        glm::mat4 view[2];
//        glm::mat4 model;
//    } uboVS;
//    vks::Buffer uniformBufferVS;

    VkPipeline m_pipeline[PSO_COUNT];
    VkPipelineLayout m_pipelineLayout[PSO_COUNT];
    //VkDescriptorSet descriptorSet;
    //VkDescriptorSetLayout m_descriptorSetLayout;

//    vr::IVRSystem *m_pHMD;
//    vr::IVRRenderModels *m_pRenderModels;
//    vr::TrackedDevicePose_t m_rTrackedDevicePose[ vr::k_unMaxTrackedDeviceCount ];
};

#endif
