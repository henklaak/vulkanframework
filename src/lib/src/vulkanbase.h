/*
* Vulkan Example base class
*
* Copyright (C) by Sascha Willems - www.saschawillems.de
*
* This code is licensed under the MIT license (MIT) (http://opensource.org/licenses/MIT)
*/

#pragma once

#if defined(VK_USE_PLATFORM_WIN32_KHR)
#pragma comment(linker, "/subsystem:windows")
#include <windows.h>
#include <fcntl.h>
#include <io.h>
#include <ShellScalingAPI.h>
#elif defined(VK_USE_PLATFORM_XCB_KHR)
#include <xcb/xcb.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <vector>
#include <array>
#include <unordered_map>
#include <numeric>
#include <ctime>
#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>
#include <sys/stat.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <numeric>
#include <array>

#include "vulkan/vulkan.h"

#include "keycodes.hpp"
#include "VulkanTools.h"
#include "VulkanDebug.h"
#include "VulkanSwapChain.h"
#include "VulkanBuffer.h"
#include "VulkanDevice.h"
#include "VulkanTexture.h"

#include "VulkanInitializers.hpp"
#include "camera.hpp"
#include "benchmark.hpp"

class VulkanBase
{
public:
    VulkanBase( bool enableValidation );
    virtual ~VulkanBase();
    /** @brief Setup the vulkan instance, enable required extensions and connect to the physical device (GPU) */
    bool initVulkan();

#if defined(VK_USE_PLATFORM_WIN32_KHR)
    void setupConsole( std::string title );
    void setupDPIAwareness();
    HWND setupWindow( HINSTANCE hinstance, WNDPROC wndproc );
    void handleMessages( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    xcb_window_t setupWindow();
    void initxcbConnection();
    void handleEvent( const xcb_generic_event_t *event );
#else
    void setupWindow();
#endif
    /** @brief (Pure virtual) Render function to be implemented by the sample application */
    virtual void render() = 0;

    /** @brief (Virtual) Creates the application wide Vulkan instance */
    virtual VkResult createInstance( bool enableValidation );

    /** @brief (Virtual) Called when the camera view has changed */
    virtual void viewChanged();

    /** @brief (Virtual) Called after a key was pressed, can be used to do custom key handling */
    virtual void keyPressed( uint32_t );

    /** @brief (Virtual) Called after the mouse cursor moved and before internal events (like camera rotation) is handled */
    virtual void mouseMoved( double x, double y, bool &handled );

    /** @brief (Virtual) Called when the window has been resized, can be used by the sample application to recreate resources */
    virtual void windowResized();

    /** @brief (Virtual) Called when resources have been recreated that require a rebuild of the command buffers (e.g. frame buffer), to be implemented by the sample application */
    virtual void buildCommandBuffers();

    /** @brief (Virtual) Setup default depth and stencil views */
    virtual void setupDepthStencil();

    /** @brief (Virtual) Setup default framebuffers for all requested swapchain images */
    virtual void setupFrameBuffer();

    /** @brief (Virtual) Setup a default renderpass */
    virtual void setupRenderPass();

    /** @brief (Virtual) Called after the physical device features have been read, can be used to set features to enable on the device */
    virtual void getEnabledFeatures();

    /** @brief Prepares all Vulkan resources and functions required to run the sample */
    virtual void prepare();

    /** @brief Loads a SPIR-V shader file for the given shader stage */
    VkPipelineShaderStageCreateInfo loadShader( std::string fileName, VkShaderStageFlagBits stage );

    /** @brief Entry point for the main render loop */
    void renderLoop();

    /** @brief Prepare the next frame for workload submission by acquiring the next swap chain image */
    void prepareFrame();

    /** @brief Presents the current image to the swap chain */
    void submitFrame();

    /** @brief (Virtual) Default image acquire + submission and command buffer submission function */
    virtual void renderFrame();

    bool m_prepared = false;
    bool m_resized = false;
    uint32_t m_width = 1280;
    uint32_t m_height = 720;

    /** @brief Antialsing factor */
    VkSampleCountFlagBits m_mssa = VK_SAMPLE_COUNT_8_BIT;

    /** @brief Last frame time measured using a high performance timer (if available) */
    float m_frameTimer = 1.0f;

    vks::Benchmark m_benchmark;

    /** @brief Encapsulated physical and logical vulkan device */
    vks::VulkanDevice *m_vulkanDevice;

    /** @brief Example settings that can be changed e.g. by command line arguments */
    struct Settings
    {
        /** @brief Activates validation layers (and message output) when set to true */
        bool validation = false;

        /** @brief Set to true if fullscreen mode has been requested via command line */
        bool fullscreen = false;

        /** @brief Set to true if v-sync will be forced for the swapchain */
        bool vsync = false;
    } m_settings;

    VkClearColorValue defaultClearColor = { { 0.1, 0.1, 0.1, 1.0} };

    // Defines a frame rate independent timer value clamped from -1.0...1.0
    // For use in animations, rotations, etc.
    float m_timer = 0.0f;
    // Multiplier for speeding up (or slowing down) the global timer
    float m_timerSpeed = 0.25f;
    bool m_paused = false;

    Camera m_camera;
    glm::vec2 m_mousePos;

    std::string m_title = "Vulkan Example";
    std::string m_name = "vulkanExample";
    uint32_t m_apiVersion = VK_API_VERSION_1_0;

    struct
    {
        VkImage image;
        VkDeviceMemory mem;
        VkImageView view;
    } m_depthStencil;

    struct
    {
        glm::vec2 axisLeft = glm::vec2( 0.0f );
        glm::vec2 axisRight = glm::vec2( 0.0f );
    } m_gamePadState;

    struct
    {
        bool left = false;
        bool right = false;
        bool middle = false;
    } m_mouseButtons;

    // OS specific
#if defined(VK_USE_PLATFORM_WIN32_KHR)
    HWND window;
    HINSTANCE windowInstance;
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    bool m_quit = false;
    xcb_connection_t *m_connection;
    xcb_screen_t *m_screen;
    xcb_window_t m_window;
    xcb_intern_atom_reply_t *m_atom_wm_delete_window;
#endif


protected:
    // Frame counter to display fps
    uint32_t m_frameCounter = 0;
    uint32_t m_lastFPS = 0;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_lastTimestamp;

    // Vulkan instance, stores all per-application states
    VkInstance m_instance;
    std::vector<std::string> m_supportedInstanceExtensions;

    // Physical device (GPU) that Vulkan will use
    VkPhysicalDevice m_physicalDevice;

    // Stores physical device properties (for e.g. checking device limits)
    VkPhysicalDeviceProperties m_deviceProperties;

    // Stores the features available on the selected physical device (for e.g. checking if a feature is available)
    VkPhysicalDeviceFeatures m_deviceFeatures;

    // Stores all available memory (type) properties for the physical device
    VkPhysicalDeviceMemoryProperties m_deviceMemoryProperties;

    /** @brief Set of physical device features to be enabled for this example (must be set in the derived constructor) */
    VkPhysicalDeviceFeatures m_enabledFeatures{};

    /** @brief Set of device extensions to be enabled for this example (must be set in the derived constructor) */
    std::vector<const char*> m_enabledDeviceExtensions;
    std::vector<const char*> m_enabledInstanceExtensions;

    /** @brief Optional pNext structure for passing extension structures to device creation */
    void* m_deviceCreatepNextChain = nullptr;

    /** @brief Logical device, application's view of the physical device (GPU) */
    VkDevice m_device;

    // Handle to the device graphics queue that command buffers are submitted to
    VkQueue m_graphicsQueue;

    // Depth buffer format (selected during Vulkan initialization)
    VkFormat m_depthFormat;

    // Command buffer pool
    VkCommandPool m_cmdPool;

    /** @brief Pipeline stages used to wait at for graphics queue submissions */
    VkPipelineStageFlags m_submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    // Contains command buffers and semaphores to be presented to the queue
    VkSubmitInfo m_submitInfo;

    // Command buffers used for rendering
    std::vector<VkCommandBuffer> m_drawCmdBuffers;

    // Global render pass for frame buffer writes
    VkRenderPass m_renderPass;

    // List of available frame buffers (same as number of swap chain images)
    std::vector<VkFramebuffer> m_frameBuffers;

    // Active frame buffer index
    uint32_t m_currentBuffer = 0;

    // Descriptor set pool
    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;

    // List of shader modules created (stored for cleanup)
    std::vector<VkShaderModule> m_shaderModules;

    // Pipeline cache object
    VkPipelineCache m_pipelineCache;

    // Wraps the swap chain to present images (framebuffers) to the windowing system
    VulkanSwapChain m_swapChain;

    // Synchronization semaphores
    struct Semaphores
    {
        // Swap chain image presentation
        VkSemaphore presentComplete;
        // Command buffer submission and execution
        VkSemaphore renderComplete;
    } m_semaphores;
    std::vector<VkFence> m_waitFences;

private:
    std::string getWindowTitle();
    void windowResize();
    void handleMouseMove( int32_t x, int32_t y );
    void nextFrame();
    void createPipelineCache();
    void createCommandPool();
    void createSynchronizationPrimitives();
    void initSwapchain();
    void setupMultisampleTarget();
    void setupSwapChain();
    void createCommandBuffers();
    void destroyCommandBuffers();

    bool m_viewUpdated = false;
    uint32_t m_destWidth = 512;
    uint32_t m_destHeight = 512;
    std::string m_shaderDir = "glsl";


    struct
    {
        struct
        {
            VkImage image;
            VkImageView view;
            VkDeviceMemory memory;
        } color;
        struct
        {
            VkImage image;
            VkImageView view;
            VkDeviceMemory memory;
        } depth;
    } m_multisampleTarget;
};

// OS specific macros for the example main entry points
#if defined(VK_USE_PLATFORM_WIN32_KHR)
// Windows entry point
#define VULKAN_EXAMPLE_MAIN()                                                                       \
VulkanExample *vulkanExample;                                                                       \
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)                        \
{                                                                                                   \
    if (vulkanExample != NULL)                                                                      \
    {                                                                                               \
        vulkanExample->handleMessages(hWnd, uMsg, wParam, lParam);                                  \
    }                                                                                               \
    return (DefWindowProc(hWnd, uMsg, wParam, lParam));                                             \
}                                                                                                   \
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)                                    \
{                                                                                                   \
    for (int32_t i = 0; i < __argc; i++) { VulkanExample::args.push_back(__argv[i]); };             \
    vulkanExample = new VulkanExample();                                                            \
    vulkanExample->initVulkan();                                                                    \
    vulkanExample->setupWindow(hInstance, WndProc);                                                 \
    vulkanExample->prepare();                                                                       \
    vulkanExample->renderLoop();                                                                    \
    delete(vulkanExample);                                                                          \
    return 0;                                                                                       \
}
#elif defined(VK_USE_PLATFORM_XCB_KHR)
#define VULKAN_MAIN()                                                                               \
VulkanExample *vulkanExample;                                                                       \
static void handleEvent(const xcb_generic_event_t *event)                                           \
{                                                                                                   \
    if (vulkanExample != NULL)                                                                      \
    {                                                                                               \
        vulkanExample->handleEvent(event);                                                          \
    }                                                                                               \
}                                                                                                   \
int main(const int argc, const char *argv[])                                                        \
{                                                                                                   \
    for (size_t i = 0; i < argc; i++) { VulkanExample::args.push_back(argv[i]); };                  \
    vulkanExample = new VulkanExample();                                                            \
    vulkanExample->initVulkan();                                                                    \
    vulkanExample->setupWindow();                                                                   \
    vulkanExample->prepare();                                                                       \
    vulkanExample->renderLoop();                                                                    \
    delete(vulkanExample);                                                                          \
    return 0;                                                                                       \
}
#else
#define VULKAN_EXAMPLE_MAIN()
#endif

