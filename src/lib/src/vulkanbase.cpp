/*
* Vulkan Example base class
*
* Copyright (C) by Sascha Willems - www.saschawillems.de
*
* This code is licensed under the MIT license (MIT) (http://opensource.org/licenses/MIT)
*/

#include "vulkanbase.h"

VkResult VulkanBase::createInstance( bool enableValidation )
{
    m_settings.validation = enableValidation;

    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = m_name.c_str();
    appInfo.pEngineName = m_name.c_str();
    appInfo.apiVersion = m_apiVersion;

    std::vector<const char*> instanceExtensions = { VK_KHR_SURFACE_EXTENSION_NAME };

    // Enable surface extensions depending on os
#if defined(VK_USE_PLATFORM_WIN32_KHR)
    instanceExtensions.push_back( VK_KHR_WIN32_SURFACE_EXTENSION_NAME );
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    instanceExtensions.push_back( VK_KHR_XCB_SURFACE_EXTENSION_NAME );
#endif

    // Get extensions supported by the instance and store for later use
    uint32_t extCount = 0;
    vkEnumerateInstanceExtensionProperties( nullptr, &extCount, nullptr );
    if( extCount > 0 )
    {
        std::vector<VkExtensionProperties> extensions( extCount );
        if( vkEnumerateInstanceExtensionProperties( nullptr, &extCount,
                &extensions.front() ) == VK_SUCCESS )
        {
            for( VkExtensionProperties extension : extensions )
            {
                m_supportedInstanceExtensions.push_back( extension.extensionName );
            }
        }
    }

    // Enabled requested instance extensions
    if( m_enabledInstanceExtensions.size() > 0 )
    {
        for( const char * enabledExtension : m_enabledInstanceExtensions )
        {
            // Output message if requested extension is not available
            if( std::find( m_supportedInstanceExtensions.begin(),
                           m_supportedInstanceExtensions.end(),
                           enabledExtension ) == m_supportedInstanceExtensions.end() )
            {
                std::cerr << "Enabled instance extension \"" << enabledExtension <<
                          "\" is not present at instance level\n";
            }
            instanceExtensions.push_back( enabledExtension );
        }
    }

    VkInstanceCreateInfo instanceCI = {};
    instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCI.pNext = NULL;
    instanceCI.pApplicationInfo = &appInfo;
    if( instanceExtensions.size() > 0 )
    {
        if( m_settings.validation )
        {
            instanceExtensions.push_back( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
        }
        instanceCI.enabledExtensionCount = ( uint32_t )instanceExtensions.size();
        instanceCI.ppEnabledExtensionNames = instanceExtensions.data();
    }

    // The VK_LAYER_KHRONOS_validation contains all current validation functionality.
    // Note that on Android this layer requires at least NDK r20
    const char* validationLayerName = "VK_LAYER_KHRONOS_validation";
    if( m_settings.validation )
    {
        // Check if this layer is available at instance level
        uint32_t instanceLayerCount;
        vkEnumerateInstanceLayerProperties( &instanceLayerCount, nullptr );
        std::vector<VkLayerProperties> instanceLayerProperties( instanceLayerCount );
        vkEnumerateInstanceLayerProperties( &instanceLayerCount, instanceLayerProperties.data() );
        bool validationLayerPresent = false;
        for( VkLayerProperties layer : instanceLayerProperties )
        {
            if( strcmp( layer.layerName, validationLayerName ) == 0 )
            {
                validationLayerPresent = true;
                break;
            }
        }
        if( validationLayerPresent )
        {
            instanceCI.ppEnabledLayerNames = &validationLayerName;
            instanceCI.enabledLayerCount = 1;
        }
        else
        {
            std::cerr << "Validation layer VK_LAYER_KHRONOS_validation not present, validation is disabled";
        }
    }
    return vkCreateInstance( &instanceCI, nullptr, &m_instance );
}

void VulkanBase::renderFrame()
{
    VulkanBase::prepareFrame();
    m_submitInfo.commandBufferCount = 1;
    m_submitInfo.pCommandBuffers = &m_drawCmdBuffers[m_currentBuffer];
    VK_CHECK_RESULT( vkQueueSubmit( m_graphicsQueue, 1, &m_submitInfo, VK_NULL_HANDLE ) );
    VulkanBase::submitFrame();
}

std::string VulkanBase::getWindowTitle()
{
    std::string device( m_deviceProperties.deviceName );
    std::string windowTitle;
    windowTitle = m_title + " - " + device;
    return windowTitle;
}

void VulkanBase::createCommandBuffers()
{
    // Create one command buffer for each swap chain image and reuse for rendering
    m_drawCmdBuffers.resize( m_swapChain.imageCount );

    VkCommandBufferAllocateInfo cmdBufAllocateInfo =
        vks::initializers::commandBufferAllocateInfo(
            m_cmdPool,
            VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            static_cast<uint32_t>( m_drawCmdBuffers.size() ) );

    VK_CHECK_RESULT( vkAllocateCommandBuffers( m_device, &cmdBufAllocateInfo,
                     m_drawCmdBuffers.data() ) );
}

void VulkanBase::destroyCommandBuffers()
{
    vkFreeCommandBuffers( m_device, m_cmdPool, static_cast<uint32_t>( m_drawCmdBuffers.size() ),
                          m_drawCmdBuffers.data() );
}

void VulkanBase::createPipelineCache()
{
    VkPipelineCacheCreateInfo pipelineCacheCI = {};
    pipelineCacheCI.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
    VK_CHECK_RESULT( vkCreatePipelineCache( m_device, &pipelineCacheCI, nullptr,
                                            &m_pipelineCache ) );
}

void VulkanBase::prepare()
{
    vks::debugmarker::setup( m_device );
    initSwapchain();
    createCommandPool();
    setupSwapChain();
    createCommandBuffers();
    createSynchronizationPrimitives();
    setupDepthStencil();
    setupRenderPass();
    createPipelineCache();
    setupMultisampleTarget();
    setupFrameBuffer();
}

VkPipelineShaderStageCreateInfo VulkanBase::loadShader( std::string fileName,
        VkShaderStageFlagBits stage )
{
    VkPipelineShaderStageCreateInfo shaderStage = {};
    shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStage.stage = stage;
    shaderStage.module = vks::tools::loadShader( fileName.c_str(), m_device );
    shaderStage.pName = "main";
    assert( shaderStage.module != VK_NULL_HANDLE );
    m_shaderModules.push_back( shaderStage.module );
    return shaderStage;
}

void VulkanBase::nextFrame()
{
#if defined(VK_USE_PLATFORM_WIN32_KHR)
    auto tStart = std::chrono::high_resolution_clock::now();
    if( m_viewUpdated )
    {
        m_viewUpdated = false;
        viewChanged();
    }

    render();
    m_frameCounter++;
    auto tEnd = std::chrono::high_resolution_clock::now();
    auto tDiff = std::chrono::duration<double, std::milli>( tEnd - tStart ).count();
    m_frameTimer = ( float )tDiff / 1000.0f;
    camera.update( m_frameTimer );
    if( camera.moving() )
    {
        m_viewUpdated = true;
    }
    // Convert to clamped timer value
    if( !m_paused )
    {
        m_timer += m_timerSpeed * m_frameTimer;
        if( m_timer > 1.0 )
        {
            m_timer -= 1.0f;
        }
    }
    float fpsTimer = ( float )( std::chrono::duration<double, std::milli>( tEnd -
                                m_lastTimestamp ).count() );
    if( fpsTimer > 1000.0f )
    {
        m_lastFPS = static_cast<uint32_t>( ( float )m_frameCounter * ( 1000.0f / fpsTimer ) );
        if( !settings.overlay )
        {
            std::string windowTitle = getWindowTitle();
            SetWindowText( window, windowTitle.c_str() );
        }
        m_frameCounter = 0;
        m_lastTimestamp = tEnd;
    }
#endif
}

void VulkanBase::renderLoop()
{
    if( m_benchmark.active )
    {
        m_benchmark.run( [ = ] { render(); }, m_vulkanDevice->properties );
        vkDeviceWaitIdle( m_device );
        if( m_benchmark.filename != "" )
        {
            m_benchmark.saveResults();
        }
        return;
    }

    m_destWidth = m_width;
    m_destHeight = m_height;
    m_lastTimestamp = std::chrono::high_resolution_clock::now();
#if defined(VK_USE_PLATFORM_WIN32_KHR)
    MSG msg;
    bool quitMessageReceived = false;
    while( !quitMessageReceived )
    {
        while( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
            if( msg.message == WM_QUIT )
            {
                quitMessageReceived = true;
                break;
            }
        }
        if( prepared && !IsIconic( window ) )
        {
            nextFrame();
        }
    }
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    xcb_flush( m_connection );
    while( !m_quit )
    {
        auto tStart = std::chrono::high_resolution_clock::now();
        if( m_viewUpdated )
        {
            m_viewUpdated = false;
            viewChanged();
        }
        xcb_generic_event_t *event;
        while( ( event = xcb_poll_for_event( m_connection ) ) )
        {
            handleEvent( event );
            free( event );
        }
        render();
        m_frameCounter++;
        auto tEnd = std::chrono::high_resolution_clock::now();
        auto tDiff = std::chrono::duration<double, std::milli>( tEnd - tStart ).count();
        m_frameTimer = tDiff / 1000.0f;
        m_camera.update( m_frameTimer );
        if( m_camera.moving() )
        {
            m_viewUpdated = true;
        }
        // Convert to clamped timer value
        if( !m_paused )
        {
            m_timer += m_timerSpeed * m_frameTimer;
            if( m_timer > 1.0 )
            {
                m_timer -= 1.0f;
            }
        }
        float fpsTimer = std::chrono::duration<double, std::milli>( tEnd - m_lastTimestamp ).count();
        if( fpsTimer > 1000.0f )
        {
            m_lastFPS = ( float )m_frameCounter * ( 1000.0f / fpsTimer );
            m_frameCounter = 0;
            m_lastTimestamp = tEnd;
        }
    }
#endif
    // Flush device to make sure all resources can be freed
    if( m_device != VK_NULL_HANDLE )
    {
        vkDeviceWaitIdle( m_device );
    }
}

void VulkanBase::prepareFrame()
{
    // Acquire the next image from the swap chain
    VkResult result = m_swapChain.acquireNextImage( m_semaphores.presentComplete, &m_currentBuffer );
    // Recreate the swapchain if it's no longer compatible with the surface (OUT_OF_DATE) or no longer optimal for presentation (SUBOPTIMAL)
    if( ( result == VK_ERROR_OUT_OF_DATE_KHR ) || ( result == VK_SUBOPTIMAL_KHR ) )
    {
        windowResize();
    }
    else
    {
        VK_CHECK_RESULT( result );
    }
}

void VulkanBase::submitFrame()
{
    VkResult result = m_swapChain.queuePresent( m_graphicsQueue, m_currentBuffer,
                      m_semaphores.renderComplete );
    if( !( ( result == VK_SUCCESS ) || ( result == VK_SUBOPTIMAL_KHR ) ) )
    {
        if( result == VK_ERROR_OUT_OF_DATE_KHR )
        {
            // Swap chain is no longer compatible with the surface and needs to be recreated
            windowResize();
            return;
        }
        else
        {
            VK_CHECK_RESULT( result );
        }
    }
    VK_CHECK_RESULT( vkQueueWaitIdle( m_graphicsQueue ) );
}

VulkanBase::VulkanBase( bool enableValidation )
{
    m_settings.validation = enableValidation;

#if defined(VK_USE_PLATFORM_XCB_KHR)
    initxcbConnection();
#endif

#if defined(VK_USE_PLATFORM_WIN32_KHR)
    // Enable console if validation is active, debug message callback will output to it
    if( this->settings.validation )
    {
        setupConsole( "Vulkan example" );
    }
    setupDPIAwareness();
#endif
}

VulkanBase::~VulkanBase()
{
    // Clean up Vulkan resources
    m_swapChain.cleanup();
    if( m_descriptorPool != VK_NULL_HANDLE )
    {
        vkDestroyDescriptorPool( m_device, m_descriptorPool, nullptr );
    }
    destroyCommandBuffers();
    vkDestroyRenderPass( m_device, m_renderPass, nullptr );
    for( uint32_t i = 0; i < m_frameBuffers.size(); i++ )
    {
        vkDestroyFramebuffer( m_device, m_frameBuffers[i], nullptr );
    }

    for( auto& shaderModule : m_shaderModules )
    {
        vkDestroyShaderModule( m_device, shaderModule, nullptr );
    }
    vkDestroyImageView( m_device, m_depthStencil.view, nullptr );
    vkDestroyImage( m_device, m_depthStencil.image, nullptr );
    vkFreeMemory( m_device, m_depthStencil.mem, nullptr );

    // Destroy MSAA target
    vkDestroyImage( m_device, m_multisampleTarget.color.image, nullptr );
    vkDestroyImageView( m_device, m_multisampleTarget.color.view, nullptr );
    vkFreeMemory( m_device, m_multisampleTarget.color.memory, nullptr );
    vkDestroyImage( m_device, m_multisampleTarget.depth.image, nullptr );
    vkDestroyImageView( m_device, m_multisampleTarget.depth.view, nullptr );
    vkFreeMemory( m_device, m_multisampleTarget.depth.memory, nullptr );

    vkDestroyPipelineCache( m_device, m_pipelineCache, nullptr );

    vkDestroyCommandPool( m_device, m_cmdPool, nullptr );

    vkDestroySemaphore( m_device, m_semaphores.presentComplete, nullptr );
    vkDestroySemaphore( m_device, m_semaphores.renderComplete, nullptr );
    for( auto& fence : m_waitFences )
    {
        vkDestroyFence( m_device, fence, nullptr );
    }

    delete m_vulkanDevice;

    if( m_settings.validation )
    {
        vks::debug::freeDebugCallback( m_instance );
    }

    vkDestroyInstance( m_instance, nullptr );

#if defined(VK_USE_PLATFORM_XCB_KHR)
    xcb_destroy_window( m_connection, m_window );
    xcb_disconnect( m_connection );
#endif
}

bool VulkanBase::initVulkan()
{
    VkResult err;

    // Vulkan instance
    err = createInstance( m_settings.validation );
    if( err )
    {
        vks::tools::exitFatal( "Could not create Vulkan instance : \n" + vks::tools::errorString( err ),
                               err );
        return false;
    }

    // If requested, we enable the default validation layers for debugging
    if( m_settings.validation )
    {
        // The report flags determine what type of messages for the layers will be displayed
        // For validating (debugging) an application the error and warning bits should suffice
        //VkDebugReportFlagsEXT debugReportFlags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
        VkDebugReportFlagsEXT debugReportFlags =
            VK_DEBUG_REPORT_ERROR_BIT_EXT                 |
            VK_DEBUG_REPORT_WARNING_BIT_EXT |
            VK_DEBUG_REPORT_INFORMATION_BIT_EXT;
        // Additional flags include performance info, loader and layer debug messages, etc.
        vks::debug::setupDebugging( m_instance, debugReportFlags, VK_NULL_HANDLE );
    }

    // Physical device
    uint32_t gpuCount = 0;
    // Get number of available physical devices
    VK_CHECK_RESULT( vkEnumeratePhysicalDevices( m_instance, &gpuCount, nullptr ) );
    if( gpuCount == 0 )
    {
        vks::tools::exitFatal( "No device with Vulkan support found", -1 );
        return false;
    }
    // Enumerate devices
    std::vector<VkPhysicalDevice> physicalDevices( gpuCount );
    err = vkEnumeratePhysicalDevices( m_instance, &gpuCount, physicalDevices.data() );
    if( err )
    {
        vks::tools::exitFatal( "Could not enumerate physical devices : \n" + vks::tools::errorString( err ),
                               err );
        return false;
    }

    // GPU selection

    // Select physical device to be used for the Vulkan example
    // Defaults to the first device unless specified by command line
    uint32_t selectedDevice = 0;

    m_physicalDevice = physicalDevices[selectedDevice];

    // Store properties (including limits), features and memory properties of the physical device (so that examples can check against them)
    vkGetPhysicalDeviceProperties( m_physicalDevice, &m_deviceProperties );
    vkGetPhysicalDeviceFeatures( m_physicalDevice, &m_deviceFeatures );
    vkGetPhysicalDeviceMemoryProperties( m_physicalDevice, &m_deviceMemoryProperties );

    // Derived examples can override this to set actual features (based on above readings) to enable for logical device creation
    getEnabledFeatures();

    // Vulkan device creation
    // This is handled by a separate class that gets a logical device representation
    // and encapsulates functions related to a device
    m_vulkanDevice = new vks::VulkanDevice( m_physicalDevice );
    VkResult res = m_vulkanDevice->createLogicalDevice(
                       m_enabledFeatures,
                       m_enabledDeviceExtensions,
                       m_deviceCreatepNextChain );
    if( res != VK_SUCCESS )
    {
        vks::tools::exitFatal( "Could not create Vulkan device: \n" + vks::tools::errorString( res ), res );
        return false;
    }
    m_device = m_vulkanDevice->logicalDevice;

    // Get a graphics queue from the device
    vkGetDeviceQueue( m_device, m_vulkanDevice->queueFamilyIndices.graphics, 0, &m_graphicsQueue );

    // Find a suitable depth format
    VkBool32 validDepthFormat = vks::tools::getSupportedDepthFormat( m_physicalDevice, &m_depthFormat );
    assert( validDepthFormat );

    m_swapChain.connect( m_instance, m_physicalDevice, m_device );

    // Create synchronization objects
    VkSemaphoreCreateInfo semaphoreCI = vks::initializers::semaphoreCreateInfo();
    // Create a semaphore used to synchronize image presentation
    // Ensures that the image is displayed before we start submitting new commands to the queue
    VK_CHECK_RESULT( vkCreateSemaphore( m_device, &semaphoreCI, nullptr,
                                        &m_semaphores.presentComplete ) );
    // Create a semaphore used to synchronize command submission
    // Ensures that the image is not presented until all commands have been submitted and executed
    VK_CHECK_RESULT( vkCreateSemaphore( m_device, &semaphoreCI, nullptr,
                                        &m_semaphores.renderComplete ) );

    // Set up submit info structure
    // Semaphores will stay the same during application lifetime
    // Command buffer submission info is set by each example
    m_submitInfo = vks::initializers::submitInfo();
    m_submitInfo.pWaitDstStageMask = &m_submitPipelineStages;
    m_submitInfo.waitSemaphoreCount = 1;
    m_submitInfo.pWaitSemaphores = &m_semaphores.presentComplete;
    m_submitInfo.signalSemaphoreCount = 1;
    m_submitInfo.pSignalSemaphores = &m_semaphores.renderComplete;

    return true;
}

#if defined(VK_USE_PLATFORM_WIN32_KHR)
// Win32 : Sets up a console window and redirects standard output to it
void VulkanBase::setupConsole( std::string title )
{
    AllocConsole();
    AttachConsole( GetCurrentProcessId() );
    FILE *stream;
    freopen_s( &stream, "CONIN$", "r", stdin );
    freopen_s( &stream, "CONOUT$", "w+", stdout );
    freopen_s( &stream, "CONOUT$", "w+", stderr );
    SetConsoleTitle( TEXT( title.c_str() ) );
}

void VulkanBase::setupDPIAwareness()
{
    typedef HRESULT *( __stdcall * SetProcessDpiAwarenessFunc )( PROCESS_DPI_AWARENESS );

    HMODULE shCore = LoadLibraryA( "Shcore.dll" );
    if( shCore )
    {
        SetProcessDpiAwarenessFunc setProcessDpiAwareness =
            ( SetProcessDpiAwarenessFunc )GetProcAddress( shCore, "SetProcessDpiAwareness" );

        if( setProcessDpiAwareness != nullptr )
        {
            setProcessDpiAwareness( PROCESS_PER_MONITOR_DPI_AWARE );
        }

        FreeLibrary( shCore );
    }
}

HWND VulkanBase::setupWindow( HINSTANCE hinstance, WNDPROC wndproc )
{
    this->windowInstance = hinstance;

    WNDCLASSEX wndClass;

    wndClass.cbSize = sizeof( WNDCLASSEX );
    wndClass.style = CS_HREDRAW | CS_VREDRAW;
    wndClass.lpfnWndProc = wndproc;
    wndClass.cbClsExtra = 0;
    wndClass.cbWndExtra = 0;
    wndClass.hInstance = hinstance;
    wndClass.hIcon = LoadIcon( NULL, IDI_APPLICATION );
    wndClass.hCursor = LoadCursor( NULL, IDC_ARROW );
    wndClass.hbrBackground = ( HBRUSH )GetStockObject( BLACK_BRUSH );
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = name.c_str();
    wndClass.hIconSm = LoadIcon( NULL, IDI_WINLOGO );

    if( !RegisterClassEx( &wndClass ) )
    {
        std::cout << "Could not register window class!\n";
        fflush( stdout );
        exit( 1 );
    }

    int screenWidth = GetSystemMetrics( SM_CXSCREEN );
    int screenHeight = GetSystemMetrics( SM_CYSCREEN );

    if( settings.fullscreen )
    {
        if( ( width != ( uint32_t )screenWidth ) && ( height != ( uint32_t )screenHeight ) )
        {
            DEVMODE dmScreenSettings;
            memset( &dmScreenSettings, 0, sizeof( dmScreenSettings ) );
            dmScreenSettings.dmSize       = sizeof( dmScreenSettings );
            dmScreenSettings.dmPelsWidth  = width;
            dmScreenSettings.dmPelsHeight = height;
            dmScreenSettings.dmBitsPerPel = 32;
            dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
            if( ChangeDisplaySettings( &dmScreenSettings, CDS_FULLSCREEN ) != DISP_CHANGE_SUCCESSFUL )
            {
                if( MessageBox( NULL, "Fullscreen Mode not supported!\n Switch to window mode?", "Error",
                                MB_YESNO | MB_ICONEXCLAMATION ) == IDYES )
                {
                    settings.fullscreen = false;
                }
                else
                {
                    return nullptr;
                }
            }
            screenWidth = width;
            screenHeight = height;
        }

    }

    DWORD dwExStyle;
    DWORD dwStyle;

    if( settings.fullscreen )
    {
        dwExStyle = WS_EX_APPWINDOW;
        dwStyle = WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
    }
    else
    {
        dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
        dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
    }

    RECT windowRect;
    windowRect.left = 0L;
    windowRect.top = 0L;
    windowRect.right = settings.fullscreen ? ( long )screenWidth : ( long )width;
    windowRect.bottom = settings.fullscreen ? ( long )screenHeight : ( long )height;

    AdjustWindowRectEx( &windowRect, dwStyle, FALSE, dwExStyle );

    std::string windowTitle = getWindowTitle();
    window = CreateWindowEx( 0,
                             name.c_str(),
                             windowTitle.c_str(),
                             dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
                             0,
                             0,
                             windowRect.right - windowRect.left,
                             windowRect.bottom - windowRect.top,
                             NULL,
                             NULL,
                             hinstance,
                             NULL );

    if( !settings.fullscreen )
    {
        // Center on screen
        uint32_t x = ( GetSystemMetrics( SM_CXSCREEN ) - windowRect.right ) / 2;
        uint32_t y = ( GetSystemMetrics( SM_CYSCREEN ) - windowRect.bottom ) / 2;
        SetWindowPos( window, 0, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE );
    }

    if( !window )
    {
        printf( "Could not create window!\n" );
        fflush( stdout );
        return nullptr;
    }

    ShowWindow( window, SW_SHOW );
    SetForegroundWindow( window );
    SetFocus( window );

    return window;
}

void VulkanBase::handleMessages( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch( uMsg )
    {
    case WM_CLOSE:
        prepared = false;
        DestroyWindow( hWnd );
        PostQuitMessage( 0 );
        break;
    case WM_PAINT:
        ValidateRect( window, NULL );
        break;
    case WM_KEYDOWN:
        switch( wParam )
        {
        case KEY_P:
            paused = !paused;
            break;
        case KEY_F1:
            if( settings.overlay )
            {
                UIOverlay.visible = !UIOverlay.visible;
            }
            break;
        case KEY_ESCAPE:
            PostQuitMessage( 0 );
            break;
        }

        if( camera.type == Camera::firstperson )
        {
            switch( wParam )
            {
            case KEY_W:
                camera.keys.up = true;
                break;
            case KEY_S:
                camera.keys.down = true;
                break;
            case KEY_A:
                camera.keys.left = true;
                break;
            case KEY_D:
                camera.keys.right = true;
                break;
            }
        }

        keyPressed( ( uint32_t )wParam );
        break;
    case WM_KEYUP:
        if( camera.type == Camera::firstperson )
        {
            switch( wParam )
            {
            case KEY_W:
                camera.keys.up = false;
                break;
            case KEY_S:
                camera.keys.down = false;
                break;
            case KEY_A:
                camera.keys.left = false;
                break;
            case KEY_D:
                camera.keys.right = false;
                break;
            }
        }
        break;
    case WM_LBUTTONDOWN:
        mousePos = glm::vec2( ( float )LOWORD( lParam ), ( float )HIWORD( lParam ) );
        mouseButtons.left = true;
        break;
    case WM_RBUTTONDOWN:
        mousePos = glm::vec2( ( float )LOWORD( lParam ), ( float )HIWORD( lParam ) );
        mouseButtons.right = true;
        break;
    case WM_MBUTTONDOWN:
        mousePos = glm::vec2( ( float )LOWORD( lParam ), ( float )HIWORD( lParam ) );
        mouseButtons.middle = true;
        break;
    case WM_LBUTTONUP:
        mouseButtons.left = false;
        break;
    case WM_RBUTTONUP:
        mouseButtons.right = false;
        break;
    case WM_MBUTTONUP:
        mouseButtons.middle = false;
        break;
    case WM_MOUSEWHEEL:
    {
        short wheelDelta = GET_WHEEL_DELTA_WPARAM( wParam );
        camera.translate( glm::vec3( 0.0f, 0.0f, ( float )wheelDelta * 0.005f ) );
        viewUpdated = true;
        break;
    }
    case WM_MOUSEMOVE:
    {
        handleMouseMove( LOWORD( lParam ), HIWORD( lParam ) );
        break;
    }
    case WM_SIZE:
        if( ( prepared ) && ( wParam != SIZE_MINIMIZED ) )
        {
            if( ( resizing ) || ( ( wParam == SIZE_MAXIMIZED ) || ( wParam == SIZE_RESTORED ) ) )
            {
                destWidth = LOWORD( lParam );
                destHeight = HIWORD( lParam );
                windowResize();
            }
        }
        break;
    case WM_GETMINMAXINFO:
    {
        LPMINMAXINFO minMaxInfo = ( LPMINMAXINFO )lParam;
        minMaxInfo->ptMinTrackSize.x = 64;
        minMaxInfo->ptMinTrackSize.y = 64;
        break;
    }
    case WM_ENTERSIZEMOVE:
        resizing = true;
        break;
    case WM_EXITSIZEMOVE:
        resizing = false;
        break;
    }
}
#elif defined(VK_USE_PLATFORM_XCB_KHR)

static inline xcb_intern_atom_reply_t* intern_atom_helper( xcb_connection_t *conn,
        bool only_if_exists, const char *str )
{
    xcb_intern_atom_cookie_t cookie = xcb_intern_atom( conn, only_if_exists, strlen( str ), str );
    return xcb_intern_atom_reply( conn, cookie, NULL );
}

// Set up a window using XCB and request event types
xcb_window_t VulkanBase::setupWindow()
{
    uint32_t value_mask, value_list[32];

    m_window = xcb_generate_id( m_connection );

    value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    value_list[0] = m_screen->black_pixel;
    value_list[1] =
        XCB_EVENT_MASK_KEY_RELEASE |
        XCB_EVENT_MASK_KEY_PRESS |
        XCB_EVENT_MASK_EXPOSURE |
        XCB_EVENT_MASK_STRUCTURE_NOTIFY |
        XCB_EVENT_MASK_POINTER_MOTION |
        XCB_EVENT_MASK_BUTTON_PRESS |
        XCB_EVENT_MASK_BUTTON_RELEASE;

    if( m_settings.fullscreen )
    {
        m_width = m_destWidth = m_screen->width_in_pixels;
        m_height = m_destHeight = m_screen->height_in_pixels;
    }

    xcb_create_window( m_connection,
                       XCB_COPY_FROM_PARENT,
                       m_window, m_screen->root,
                       0, 0, m_width, m_height, 0,
                       XCB_WINDOW_CLASS_INPUT_OUTPUT,
                       m_screen->root_visual,
                       value_mask, value_list );

    /* Magic code that will send notification when window is destroyed */
    xcb_intern_atom_reply_t* reply = intern_atom_helper( m_connection, true, "WM_PROTOCOLS" );
    m_atom_wm_delete_window = intern_atom_helper( m_connection, false, "WM_DELETE_WINDOW" );

    xcb_change_property( m_connection, XCB_PROP_MODE_REPLACE,
                         m_window, ( *reply ).atom, 4, 32, 1,
                         &( *m_atom_wm_delete_window ).atom );

    std::string windowTitle = getWindowTitle();
    xcb_change_property( m_connection, XCB_PROP_MODE_REPLACE,
                         m_window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
                         m_title.size(), windowTitle.c_str() );

    free( reply );

    /**
     * Set the WM_CLASS property to display
     * title in dash tooltip and application menu
     * on GNOME and other desktop environments
     */
    std::string wm_class;
    wm_class = wm_class.insert( 0, m_name );
    wm_class = wm_class.insert( m_name.size(), 1, '\0' );
    wm_class = wm_class.insert( m_name.size() + 1, m_title );
    wm_class = wm_class.insert( wm_class.size(), 1, '\0' );
    xcb_change_property( m_connection, XCB_PROP_MODE_REPLACE, m_window, XCB_ATOM_WM_CLASS,
                         XCB_ATOM_STRING,
                         8, wm_class.size() + 2, wm_class.c_str() );

    if( m_settings.fullscreen )
    {
        xcb_intern_atom_reply_t *atom_wm_state = intern_atom_helper( m_connection, false, "_NET_WM_STATE" );
        xcb_intern_atom_reply_t *atom_wm_fullscreen = intern_atom_helper( m_connection, false,
                "_NET_WM_STATE_FULLSCREEN" );
        xcb_change_property( m_connection,
                             XCB_PROP_MODE_REPLACE,
                             m_window, atom_wm_state->atom,
                             XCB_ATOM_ATOM, 32, 1,
                             &( atom_wm_fullscreen->atom ) );
        free( atom_wm_fullscreen );
        free( atom_wm_state );
    }

    xcb_map_window( m_connection, m_window );

    return( m_window );
}

// Initialize XCB connection
void VulkanBase::initxcbConnection()
{
    const xcb_setup_t *setup;
    xcb_screen_iterator_t iter;
    int scr;

    // xcb_connect always returns a non-NULL pointer to a xcb_connection_t,
    // even on failure. Callers need to use xcb_connection_has_error() to
    // check for failure. When finished, use xcb_disconnect() to close the
    // connection and free the structure.
    m_connection = xcb_connect( NULL, &scr );
    assert( m_connection );
    if( xcb_connection_has_error( m_connection ) )
    {
        printf( "Could not find a compatible Vulkan ICD!\n" );
        fflush( stdout );
        exit( 1 );
    }

    setup = xcb_get_setup( m_connection );
    iter = xcb_setup_roots_iterator( setup );
    while( scr-- > 0 )
    {
        xcb_screen_next( &iter );
    }
    m_screen = iter.data;
}

void VulkanBase::handleEvent( const xcb_generic_event_t *event )
{
    switch( event->response_type & 0x7f )
    {
    case XCB_CLIENT_MESSAGE:
        if( ( *( xcb_client_message_event_t* )event ).data.data32[0] ==
                ( *m_atom_wm_delete_window ).atom )
        {
            m_quit = true;
        }
        break;
    case XCB_MOTION_NOTIFY:
    {
        xcb_motion_notify_event_t *motion = ( xcb_motion_notify_event_t * )event;
        handleMouseMove( ( int32_t )motion->event_x, ( int32_t )motion->event_y );
        break;
    }
    break;
    case XCB_BUTTON_PRESS:
    {
        xcb_button_press_event_t *press = ( xcb_button_press_event_t * )event;
        if( press->detail == XCB_BUTTON_INDEX_1 )
        {
            m_mouseButtons.left = true;
        }
        if( press->detail == XCB_BUTTON_INDEX_2 )
        {
            m_mouseButtons.middle = true;
        }
        if( press->detail == XCB_BUTTON_INDEX_3 )
        {
            m_mouseButtons.right = true;
        }
    }
    break;
    case XCB_BUTTON_RELEASE:
    {
        xcb_button_press_event_t *press = ( xcb_button_press_event_t * )event;
        if( press->detail == XCB_BUTTON_INDEX_1 )
        {
            m_mouseButtons.left = false;
        }
        if( press->detail == XCB_BUTTON_INDEX_2 )
        {
            m_mouseButtons.middle = false;
        }
        if( press->detail == XCB_BUTTON_INDEX_3 )
        {
            m_mouseButtons.right = false;
        }
    }
    break;
    case XCB_KEY_PRESS:
    {
        const xcb_key_release_event_t *keyEvent = ( const xcb_key_release_event_t * )event;
        switch( keyEvent->detail )
        {
        case KEY_W:
            m_camera.keys.up = true;
            break;
        case KEY_S:
            m_camera.keys.down = true;
            break;
        case KEY_A:
            m_camera.keys.left = true;
            break;
        case KEY_D:
            m_camera.keys.right = true;
            break;
        case KEY_P:
            m_paused = !m_paused;
            break;
        }
    }
    break;
    case XCB_KEY_RELEASE:
    {
        const xcb_key_release_event_t *keyEvent = ( const xcb_key_release_event_t * )event;
        switch( keyEvent->detail )
        {
        case KEY_W:
            m_camera.keys.up = false;
            break;
        case KEY_S:
            m_camera.keys.down = false;
            break;
        case KEY_A:
            m_camera.keys.left = false;
            break;
        case KEY_D:
            m_camera.keys.right = false;
            break;
        case KEY_ESCAPE:
            m_quit = true;
            break;
        }
        keyPressed( keyEvent->detail );
    }
    break;
    case XCB_DESTROY_NOTIFY:
        m_quit = true;
        break;
    case XCB_CONFIGURE_NOTIFY:
    {
        const xcb_configure_notify_event_t *cfgEvent = ( const xcb_configure_notify_event_t * )event;
        if( ( m_prepared ) && ( ( cfgEvent->width != m_width ) || ( cfgEvent->height != m_height ) ) )
        {
            m_destWidth = cfgEvent->width;
            m_destHeight = cfgEvent->height;
            if( ( m_destWidth > 0 ) && ( m_destHeight > 0 ) )
            {
                windowResize();
            }
        }
    }
    break;
    default:
        break;
    }
}
#else
void VulkanBase::setupWindow()
{
}
#endif

void VulkanBase::viewChanged() {}

void VulkanBase::keyPressed( uint32_t ) {}

void VulkanBase::mouseMoved( double x, double y, bool & handled ) {}

void VulkanBase::buildCommandBuffers() {}

void VulkanBase::createSynchronizationPrimitives()
{
    // Wait fences to sync command buffer access
    VkFenceCreateInfo fenceCI = vks::initializers::fenceCreateInfo(
                                    VK_FENCE_CREATE_SIGNALED_BIT );
    m_waitFences.resize( m_drawCmdBuffers.size() );
    for( auto& fence : m_waitFences )
    {
        VK_CHECK_RESULT( vkCreateFence( m_device, &fenceCI, nullptr, &fence ) );
    }
}

void VulkanBase::createCommandPool()
{
    VkCommandPoolCreateInfo cmdPoolInfo = {};
    cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdPoolInfo.queueFamilyIndex = m_swapChain.queueNodeIndex;
    cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    VK_CHECK_RESULT( vkCreateCommandPool( m_device, &cmdPoolInfo, nullptr, &m_cmdPool ) );
}

void VulkanBase::setupDepthStencil()
{
    VkImageCreateInfo imageCI{};
    imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCI.imageType = VK_IMAGE_TYPE_2D;
    imageCI.format = m_depthFormat;
    imageCI.extent = { m_width, m_height, 1 };
    imageCI.mipLevels = 1;
    imageCI.arrayLayers = 1;
    imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageCI.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

    VK_CHECK_RESULT( vkCreateImage( m_device, &imageCI, nullptr, &m_depthStencil.image ) );
    VkMemoryRequirements memReqs{};
    vkGetImageMemoryRequirements( m_device, m_depthStencil.image, &memReqs );

    VkMemoryAllocateInfo memAllloc{};
    memAllloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memAllloc.allocationSize = memReqs.size;
    memAllloc.memoryTypeIndex = m_vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );
    VK_CHECK_RESULT( vkAllocateMemory( m_device, &memAllloc, nullptr, &m_depthStencil.mem ) );
    VK_CHECK_RESULT( vkBindImageMemory( m_device, m_depthStencil.image, m_depthStencil.mem, 0 ) );

    VkImageViewCreateInfo imageViewCI{};
    imageViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imageViewCI.image = m_depthStencil.image;
    imageViewCI.format = m_depthFormat;
    imageViewCI.subresourceRange.baseMipLevel = 0;
    imageViewCI.subresourceRange.levelCount = 1;
    imageViewCI.subresourceRange.baseArrayLayer = 0;
    imageViewCI.subresourceRange.layerCount = 1;
    imageViewCI.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    // Stencil aspect should only be set on depth + stencil formats (VK_FORMAT_D16_UNORM_S8_UINT..VK_FORMAT_D32_SFLOAT_S8_UINT
    if( m_depthFormat >= VK_FORMAT_D16_UNORM_S8_UINT )
    {
        imageViewCI.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
    }
    VK_CHECK_RESULT( vkCreateImageView( m_device, &imageViewCI, nullptr, &m_depthStencil.view ) );
}

// Creates a multi sample render target (image and view) that is used to resolve
// into the visible frame buffer target in the render pass
void VulkanBase::setupMultisampleTarget()
{
    // Color target
    VkImageCreateInfo info = vks::initializers::imageCreateInfo();
    info.imageType = VK_IMAGE_TYPE_2D;
    info.format = m_swapChain.colorFormat;
    info.extent.width = m_width;
    info.extent.height = m_height;
    info.extent.depth = 1;
    info.mipLevels = 1;
    info.arrayLayers = 1;
    info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    info.tiling = VK_IMAGE_TILING_OPTIMAL;
    info.samples = m_mssa;
    // Image will only be used as a transient target
    info.usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VK_CHECK_RESULT( vkCreateImage( m_device, &info, nullptr, &m_multisampleTarget.color.image ) );

    VkMemoryRequirements memReqs;
    vkGetImageMemoryRequirements( m_device, m_multisampleTarget.color.image, &memReqs );
    VkMemoryAllocateInfo memAlloc = vks::initializers::memoryAllocateInfo();
    memAlloc.allocationSize = memReqs.size;
    // We prefer a lazily allocated memory type
    // This means that the memory gets allocated when the implementation sees fit, e.g. when first using the images
    VkBool32 lazyMemTypePresent;
    memAlloc.memoryTypeIndex = m_vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                               VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT, &lazyMemTypePresent );
    if( !lazyMemTypePresent )
    {
        // If this is not available, fall back to device local memory
        memAlloc.memoryTypeIndex = m_vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                                   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );
    }
    VK_CHECK_RESULT( vkAllocateMemory( m_device, &memAlloc, nullptr,
                                       &m_multisampleTarget.color.memory ) );
    vkBindImageMemory( m_device, m_multisampleTarget.color.image, m_multisampleTarget.color.memory, 0 );

    // Create image view for the MSAA target
    VkImageViewCreateInfo viewInfo = vks::initializers::imageViewCreateInfo();
    viewInfo.image = m_multisampleTarget.color.image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = m_swapChain.colorFormat;
    viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    viewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    viewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    viewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.layerCount = 1;

    VK_CHECK_RESULT( vkCreateImageView( m_device, &viewInfo, nullptr,
                                        &m_multisampleTarget.color.view ) );

    // Depth target
    info.imageType = VK_IMAGE_TYPE_2D;
    info.format = m_depthFormat;
    info.extent.width = m_width;
    info.extent.height = m_height;
    info.extent.depth = 1;
    info.mipLevels = 1;
    info.arrayLayers = 1;
    info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    info.tiling = VK_IMAGE_TILING_OPTIMAL;
    info.samples = m_mssa;
    // Image will only be used as a transient target
    info.usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VK_CHECK_RESULT( vkCreateImage( m_device, &info, nullptr, &m_multisampleTarget.depth.image ) );

    vkGetImageMemoryRequirements( m_device, m_multisampleTarget.depth.image, &memReqs );
    memAlloc = vks::initializers::memoryAllocateInfo();
    memAlloc.allocationSize = memReqs.size;

    memAlloc.memoryTypeIndex = m_vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                               VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT, &lazyMemTypePresent );
    if( !lazyMemTypePresent )
    {
        memAlloc.memoryTypeIndex = m_vulkanDevice->getMemoryType( memReqs.memoryTypeBits,
                                   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );
    }

    VK_CHECK_RESULT( vkAllocateMemory( m_device, &memAlloc, nullptr,
                                       &m_multisampleTarget.depth.memory ) );
    vkBindImageMemory( m_device, m_multisampleTarget.depth.image, m_multisampleTarget.depth.memory, 0 );

    // Create image view for the MSAA target
    viewInfo.image = m_multisampleTarget.depth.image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = m_depthFormat;
    viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    viewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    viewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    viewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.layerCount = 1;

    VK_CHECK_RESULT( vkCreateImageView( m_device, &viewInfo, nullptr,
                                        &m_multisampleTarget.depth.view ) );
}

void VulkanBase::setupFrameBuffer()
{

    std::array<VkImageView, 3> attachments;

    attachments[0] = m_multisampleTarget.color.view;
    attachments[1] = m_multisampleTarget.depth.view;

    VkFramebufferCreateInfo frameBufferCI = {};
    frameBufferCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    frameBufferCI.pNext = NULL;
    frameBufferCI.renderPass = m_renderPass;
    frameBufferCI.attachmentCount = attachments.size();
    frameBufferCI.pAttachments = attachments.data();
    frameBufferCI.width = m_width;
    frameBufferCI.height = m_height;
    frameBufferCI.layers = 1;

    // Create frame buffers for every swap chain image
    m_frameBuffers.resize( m_swapChain.imageCount );
    for( uint32_t i = 0; i < m_frameBuffers.size(); i++ )
    {
        attachments[2] = m_swapChain.buffers[i].view;
        VK_CHECK_RESULT( vkCreateFramebuffer( m_device, &frameBufferCI, nullptr,
                                              &m_frameBuffers[i] ) );
    }
}

void VulkanBase::setupRenderPass()
{
    std::array<VkAttachmentDescription, 3> attachments = {};
    // Color attachment
    attachments[0].format = m_swapChain.colorFormat;
    attachments[0].samples = m_mssa;
    attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    // Depth attachment
    attachments[1].format = m_depthFormat;
    attachments[1].samples = m_mssa;
    attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    // Resolve attachment
    attachments[2].format = m_swapChain.colorFormat;
    attachments[2].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[2].loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[2].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[2].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[2].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[2].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[2].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference colorReference = {};
    colorReference.attachment = 0;
    colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthReference = {};
    depthReference.attachment = 1;
    depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference resolveRef{};
    resolveRef.attachment = 2;
    resolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpassDescription = {};
    subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDescription.colorAttachmentCount = 1;
    subpassDescription.pColorAttachments = &colorReference;
    subpassDescription.pDepthStencilAttachment = &depthReference;
    subpassDescription.pResolveAttachments = &resolveRef;
    subpassDescription.inputAttachmentCount = 0;
    subpassDescription.pInputAttachments = nullptr;
    subpassDescription.preserveAttachmentCount = 0;
    subpassDescription.pPreserveAttachments = nullptr;

    // Subpass dependencies for layout transitions
    std::array<VkSubpassDependency, 2> dependencies;

    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = attachments.size();
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpassDescription;
    renderPassInfo.dependencyCount = dependencies.size();
    renderPassInfo.pDependencies = dependencies.data();

    VK_CHECK_RESULT( vkCreateRenderPass( m_device, &renderPassInfo, nullptr, &m_renderPass ) );
}

void VulkanBase::getEnabledFeatures() {}

void VulkanBase::windowResize()
{
    if( !m_prepared )
    {
        return;
    }
    m_prepared = false;
    m_resized = true;

    // Ensure all operations on the device have been finished before destroying resources
    vkDeviceWaitIdle( m_device );

    // Recreate swap chain
    m_width = m_destWidth;
    m_height = m_destHeight;
    setupSwapChain();

    // Recreate the frame buffers
    vkDestroyImageView( m_device, m_depthStencil.view, nullptr );
    vkDestroyImage( m_device, m_depthStencil.image, nullptr );
    vkFreeMemory( m_device, m_depthStencil.mem, nullptr );
    setupDepthStencil();
    for( uint32_t i = 0; i < m_frameBuffers.size(); i++ )
    {
        vkDestroyFramebuffer( m_device, m_frameBuffers[i], nullptr );
    }
    setupMultisampleTarget();

    setupFrameBuffer();

    // Command buffers need to be recreated as they may store
    // references to the recreated frame buffer
    destroyCommandBuffers();
    createCommandBuffers();
    buildCommandBuffers();

    vkDeviceWaitIdle( m_device );

    if( ( m_width > 0.0f ) && ( m_height > 0.0f ) )
    {
        m_camera.updateAspectRatio( ( float )m_width / ( float )m_height );
    }

    // Notify derived class
    windowResized();
    viewChanged();

    m_prepared = true;
}

void VulkanBase::handleMouseMove( int32_t x, int32_t y )
{
    int32_t dx = ( int32_t )m_mousePos.x - x;
    int32_t dy = ( int32_t )m_mousePos.y - y;

    bool handled = false;

    mouseMoved( ( float )x, ( float )y, handled );

    if( handled )
    {
        m_mousePos = glm::vec2( ( float )x, ( float )y );
        return;
    }

    if( m_mouseButtons.left )
    {
        m_camera.rotate( glm::vec3( dy * m_camera.rotationSpeed, -dx * m_camera.rotationSpeed, 0.0f ) );
        m_viewUpdated = true;
    }
    if( m_mouseButtons.right )
    {
        m_camera.translate( glm::vec3( -0.0f, 0.0f, dy * .005f ) );
        m_viewUpdated = true;
    }
    if( m_mouseButtons.middle )
    {
        m_camera.translate( glm::vec3( -dx * 0.01f, -dy * 0.01f, 0.0f ) );
        m_viewUpdated = true;
    }
    m_mousePos = glm::vec2( ( float )x, ( float )y );
}

void VulkanBase::windowResized() {}

void VulkanBase::initSwapchain()
{
#if defined(VK_USE_PLATFORM_WIN32_KHR)
    swapChain.initSurface( windowInstance, window );
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    m_swapChain.initSurface( m_connection, m_window );
#endif
}

void VulkanBase::setupSwapChain()
{
    m_swapChain.create( &m_width, &m_height, m_settings.vsync );
}

